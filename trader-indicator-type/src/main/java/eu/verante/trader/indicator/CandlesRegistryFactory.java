package eu.verante.trader.indicator;

import eu.verante.trader.stock.candle.Period;

import java.time.ZonedDateTime;

public interface CandlesRegistryFactory {
    CandlesRegistry prepareRegistry(String symbol, Period period, ZonedDateTime from, ZonedDateTime to);
}
