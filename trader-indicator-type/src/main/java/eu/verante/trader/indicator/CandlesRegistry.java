package eu.verante.trader.indicator;

import eu.verante.trader.stock.candle.TradingCandle;

import java.util.Collection;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

public class CandlesRegistry implements Iterable<TradingCandle>{

    private final SortedMap<Long, TradingCandle> cachedCandles;

    public CandlesRegistry(Collection<TradingCandle> candles) {
        cachedCandles = new TreeMap<>();
        candles.forEach(c -> cachedCandles.put(c.time().toInstant().toEpochMilli(), c));
    }

    @Override
    public Iterator<TradingCandle> iterator() {
        return cachedCandles.values().iterator();
    }
}
