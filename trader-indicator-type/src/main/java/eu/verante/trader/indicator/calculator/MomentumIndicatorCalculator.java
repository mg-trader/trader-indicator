package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.SimplePeriodsConfiguration;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;

@Scope("prototype")
@Component
public class MomentumIndicatorCalculator extends AbstractIndicatorCalculator<SimplePeriodsConfiguration, SimpleIndicatorValue<Double>> {

    private int periods;
    @Override
    public void initialize(SimplePeriodsConfiguration configuration) {
        this.periods = configuration.getPeriods();
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);

        CircularFifoQueue<Double> candlesQueue = new CircularFifoQueue<>(periods);

        for (TradingCandle tradingCandle : candlesRegistry) {
            double currentValue = tradingCandle.close().doubleValue();
            candlesQueue.add(currentValue);
            if (candlesQueue.size() < periods) {
                values.put(tradingCandle.time(), createEmptyValue());
            } else {
                double prePeriodsValue = candlesQueue.get(0);
                double result = 100.0+ (100.0*(currentValue - prePeriodsValue)/prePeriodsValue);
                values.put(tradingCandle.time(), SimpleIndicatorValue.simple(result));
            }
        }
        return values;
    }
}
