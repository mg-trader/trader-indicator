package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static java.lang.Math.round;
import static java.lang.Math.sqrt;

@Scope("prototype")
@Component
public class HullMAIndicatorCalculator extends AbstractIndicatorCalculator
        <MAIndicatorConfiguration, SimpleIndicatorValue<Double>> {

    @Autowired
    private WMAIndicatorCalculator wmaFullPeriodCalculator;

    @Autowired
    private WMAIndicatorCalculator wmaHalfPeriodCalculator;

    @Autowired
    private WMAIndicatorCalculator wmaSquareRootPeriodCalculator;

    private int shift;
    private SimpleValueTypeDefinition base;

    @Override
    public void initialize(MAIndicatorConfiguration configuration) {
        this.shift = configuration.getShift();
        this.base = configuration.getBase();
        int periods = configuration.getPeriods();

        wmaFullPeriodCalculator.initialize(MAIndicatorConfiguration.builder()
                .periods(periods)
                .base(base)
                .build());

        wmaHalfPeriodCalculator.initialize(MAIndicatorConfiguration.builder()
                .periods(periods/2)
                .base(base)
                .build());

        wmaSquareRootPeriodCalculator.initialize(MAIndicatorConfiguration.builder()
                .periods(Long.valueOf(round(sqrt(periods))).intValue())
                .base(base)
                .build());

    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> fullPeriodValues = wmaFullPeriodCalculator.doCalculate(candlesRegistry);
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> halfPeriodValues = wmaHalfPeriodCalculator.doCalculate(candlesRegistry);

        SortedMap<ZonedDateTime, Double> rawHull = new TreeMap<>();
        fullPeriodValues.keySet().forEach(date ->
                rawHull.put(date, 2*halfPeriodValues.get(date).getValue() - fullPeriodValues.get(date).getValue()));

        return shift(wmaSquareRootPeriodCalculator.doCalculate(rawHull), shift);
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }
}
