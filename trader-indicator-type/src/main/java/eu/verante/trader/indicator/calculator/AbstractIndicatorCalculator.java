package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.*;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;
import eu.verante.trader.stock.candle.TradingCandle;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;

public abstract class AbstractIndicatorCalculator<C extends IndicatorConfiguration, V extends IndicatorValue>
        implements IndicatorCalculator<V> {

    @Autowired
    private CandlesRegistryFactory candlesRegistryFactory;

    @Autowired
    private IndicatorValueRegistryFactory indicatorValueRegistryFactory;

    private static final Map<SimpleValueTypeDefinition,
            Function<TradingCandle, BigDecimal>> valueGetters
            = initializeValueGetters();

    public abstract void initialize(C configuration);

    protected CandlesRegistry prepareRegistry(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        return candlesRegistryFactory.prepareRegistry(symbol, period, from, to);
    }

    protected <VO extends IndicatorValue> IndicatorValueRegistry<VO> prepareRegistry(String indicator, String symbol, Period period,
                                                                                     ZonedDateTime from, ZonedDateTime to) {
        return indicatorValueRegistryFactory.prepareRegistry(indicator,symbol, period, from, to);
    }

    protected Map<ZonedDateTime, V> shift(Map<ZonedDateTime, V> input, int periods) {
        if (periods == 0) {
            return input;
        }
        SortedMap<ZonedDateTime, V> sortedInput = new TreeMap<>(input);
        List<ZonedDateTime> keys = new ArrayList<>(sortedInput.keySet());
        List<V> values = new ArrayList<>(sortedInput.values());

        Map<ZonedDateTime, V> result = new TreeMap<>();

        List<ZonedDateTime> shiftedKeys;
        List<ZonedDateTime> missingKeys;
        List<V> shiftedValues;
        if (periods > 0) {
            shiftedKeys = keys.subList(periods, keys.size());
            missingKeys = keys.subList(0, periods);
            shiftedValues = values;
        } else {
            shiftedKeys = keys.subList(0, keys.size()+periods);
            missingKeys = keys.subList(keys.size()+periods, keys.size());
            shiftedValues = values.subList(Math.abs(periods), keys.size());
        }
        for (int index = 0; index < shiftedKeys.size(); index++) {
            result.put(shiftedKeys.get(index), shiftedValues.get(index));
        }
        missingKeys.forEach(k -> result.put(k, createEmptyValue()));

        return result;
    }

    protected abstract V createEmptyValue();

    protected Function<TradingCandle, BigDecimal> getValueGetter(SimpleValueTypeDefinition type) {
        return valueGetters.get(type);
    }

    private static Map<SimpleValueTypeDefinition, Function<TradingCandle, BigDecimal>> initializeValueGetters() {
        return Map.of(
                SimpleValueTypeDefinition.OPEN,TradingCandle::open,
                SimpleValueTypeDefinition.CLOSE,TradingCandle::close,
                SimpleValueTypeDefinition.HIGH,TradingCandle::high,
                SimpleValueTypeDefinition.LOW,TradingCandle::low,
                SimpleValueTypeDefinition.TYPICAL,AbstractIndicatorCalculator::typical,
                SimpleValueTypeDefinition.VOLUME,TradingCandle::volume
        );
    }

    protected static BigDecimal typical(TradingCandle candle) {
        BigDecimal value = candle.low().add(candle.high()).add(candle.close());
        return value.divide(BigDecimal.valueOf(3), 4, RoundingMode.HALF_UP);
    }

}
