package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

@Scope("prototype")
@Component
public class EMAIndicatorCalculator extends AbstractIndicatorCalculator
        <MAIndicatorConfiguration, SimpleIndicatorValue<Double>> {
    private int periods;
    private int shift;
    private SimpleValueTypeDefinition base;

    @Override
    public void initialize(MAIndicatorConfiguration configuration) {
        this.periods = configuration.getPeriods();
        this.shift = configuration.getShift();
        this.base = configuration.getBase();
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        Function<TradingCandle, BigDecimal> valueGetter = getValueGetter(base);
        double alpha = 2.0 /(periods + 1.0);

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);

        double lastResult = valueGetter.apply(candlesRegistry.iterator().next()).doubleValue();

        for (TradingCandle tradingCandle : candlesRegistry) {

            double newResult = alpha * valueGetter.apply(tradingCandle).doubleValue() + (1.0 - alpha)* lastResult;
            values.put(tradingCandle.time(), SimpleIndicatorValue.simple(newResult));
            lastResult = newResult;
        }
        return shift(values, shift);
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }
}
