package eu.verante.trader.indicator.type;

import eu.verante.trader.indicator.calculator.*;
import eu.verante.trader.indicator.model.configuration.*;
import eu.verante.trader.indicator.model.item.IndicatorValueTypeDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
public class SpringIndicatorConfig implements IndicatorConfigurationKeys {

    @Bean
    public IndicatorTypeRegistry getIndicatorTypeRegistry() {
        return IndicatorTypeRegistry.builder()
                .with(new IndicatorTypeDefinition("SIMPLE", IndicatorCategoryDefinition.SIMPLE,
                        "Simple value", IndicatorValueTypeDefinition.SIMPLE,
                        SimpleIndicatorConfiguration.class, List.of(VALUE),
                        SimpleIndicatorCalculator.class))
                .with(new IndicatorTypeDefinition("SMA", IndicatorCategoryDefinition.MOVING_AVERAGE,
                        "Simple Moving Average", IndicatorValueTypeDefinition.SIMPLE,
                        MAIndicatorConfiguration.class, List.of(PERIODS),
                        SMAIndicatorCalculator.class))
                .with(new IndicatorTypeDefinition("EMA", IndicatorCategoryDefinition.MOVING_AVERAGE,
                        "Exponential Moving Average", IndicatorValueTypeDefinition.SIMPLE,
                        MAIndicatorConfiguration.class, List.of(PERIODS),
                        EMAIndicatorCalculator.class))
                .with(new IndicatorTypeDefinition("WMA", IndicatorCategoryDefinition.MOVING_AVERAGE,
                        "Weighted Moving Average", IndicatorValueTypeDefinition.SIMPLE,
                        MAIndicatorConfiguration.class, List.of(PERIODS),
                        WMAIndicatorCalculator.class))
                .with(new IndicatorTypeDefinition("HULL_MA", IndicatorCategoryDefinition.MOVING_AVERAGE,
                        "Hull Moving Average", IndicatorValueTypeDefinition.SIMPLE,
                        MAIndicatorConfiguration.class, List.of(PERIODS),
                        HullMAIndicatorCalculator.class))

                .with(new IndicatorTypeDefinition("TREND", IndicatorCategoryDefinition.TREND,
                        "Trend indicator", IndicatorValueTypeDefinition.TREND,
                        TrendIndicatorConfiguration.class, List.of(INDICATOR, THRESHOLD),
                        TrendIndicatorCalculator.class))
                .with(new IndicatorTypeDefinition("COMPARE_INDICATORS", IndicatorCategoryDefinition.COMPARE,
                        "Compare two indicators", IndicatorValueTypeDefinition.COMPARE,
                        CompareIndicatorConfiguration.class, List.of(VALUE, COMPARE_TO),
                        CompareIndicatorCalculator.class))

                .with(new IndicatorTypeDefinition("MOMENTUM", IndicatorCategoryDefinition.OSCILLATOR,
                        "Momentum oscillator", IndicatorValueTypeDefinition.SIMPLE,
                        SimplePeriodsConfiguration.class, List.of(PERIODS),
                        MomentumIndicatorCalculator.class
                        ))
                .with(new IndicatorTypeDefinition("CCI", IndicatorCategoryDefinition.OSCILLATOR,
                        "Commodity Channel Index oscillator", IndicatorValueTypeDefinition.SIMPLE,
                        SimplePeriodsConfiguration.class, List.of(PERIODS),
                        CCIIndicatorCalculator.class))

                .with(new IndicatorTypeDefinition("RSI", IndicatorCategoryDefinition.OSCILLATOR,
                        "Relative Strength Index", IndicatorValueTypeDefinition.SIMPLE,
                        SimplePeriodsConfiguration.class, List.of(PERIODS),
                        RSIIndicatorCalculator.class))
                .build();
    }
}
