package eu.verante.trader.indicator;

import eu.verante.trader.indicator.domain.value.IndicatorValue;

import java.util.Collections;
import java.util.SortedMap;

public class IndicatorValueRegistry<V extends IndicatorValue> {

    private final SortedMap<Long, V> cachedValues;

    public IndicatorValueRegistry(SortedMap<Long, V> cachedValues) {
        this.cachedValues = Collections.unmodifiableSortedMap(cachedValues);
    }

    public SortedMap<Long, V> getCachedValues() {
        return cachedValues;
    }
}
