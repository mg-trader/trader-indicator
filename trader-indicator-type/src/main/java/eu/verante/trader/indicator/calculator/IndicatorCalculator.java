package eu.verante.trader.indicator.calculator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.domain.value.IndicatorValue;

import java.time.ZonedDateTime;
import java.util.Map;

public interface IndicatorCalculator<V extends IndicatorValue> {

    Map<ZonedDateTime, V> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to);
}
