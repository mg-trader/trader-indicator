package eu.verante.trader.indicator.type;

import eu.verante.trader.indicator.calculator.IndicatorCalculator;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IndicatorTypeRegistry {

    private final Map<String, IndicatorTypeDefinition> indicators;

    private IndicatorTypeRegistry(Map<String, IndicatorTypeDefinition> indicators) {
        this.indicators = Collections.unmodifiableMap(indicators);
    }

    public <C extends IndicatorConfiguration,
            V extends IndicatorValue,
            CA extends IndicatorCalculator<V>> IndicatorTypeDefinition<C, V, CA> getByName(String name) {
        return (IndicatorTypeDefinition<C, V, CA>) indicators.get(name);
    }

    public Collection<IndicatorTypeDefinition> getAll() {
        return indicators.values();
    }

    public static Builder builder() {
        return new Builder();
    }

    static class Builder {

        private final Map<String, IndicatorTypeDefinition> indicators;

        private Builder() {
            indicators = new HashMap<>();
        }

        public Builder with(IndicatorTypeDefinition indicatorType) {
            this.indicators.put(indicatorType.getType(), indicatorType);
            return this;
        }

        public IndicatorTypeRegistry build() {
            return new IndicatorTypeRegistry(indicators);
        }
    }
}
