package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

@Scope("prototype")
@Component
public class SMAIndicatorCalculator extends AbstractIndicatorCalculator
        <MAIndicatorConfiguration, SimpleIndicatorValue<Double>> {

    private int periods;
    private int shift;
    private SimpleValueTypeDefinition base;

    @Override
    public void initialize(MAIndicatorConfiguration configuration) {
        this.periods = configuration.getPeriods();
        this.shift = configuration.getShift();
        this.base = configuration.getBase();
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);

        return doCalculate(candlesRegistry);
    }

    Map<ZonedDateTime, SimpleIndicatorValue<Double>> doCalculate(CandlesRegistry candlesRegistry) {
        Function<TradingCandle, BigDecimal> valueGetter = getValueGetter(base);

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();
        CircularFifoQueue<Double> candlesQueue = new CircularFifoQueue<>(periods);
        for (TradingCandle tradingCandle : candlesRegistry) {
            candlesQueue.add(valueGetter.apply(tradingCandle).doubleValue());
            double sum = candlesQueue.stream().mapToDouble(Double::doubleValue).sum();
            values.put(tradingCandle.time(), SimpleIndicatorValue.simple(sum/candlesQueue.size()));
        }
        return shift(values, shift);
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }
}
