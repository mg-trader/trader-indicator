package eu.verante.trader.indicator.calculator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.IndicatorValueRegistry;
import eu.verante.trader.indicator.domain.value.CompareIndicatorValue;
import eu.verante.trader.indicator.domain.value.CompareState;
import eu.verante.trader.indicator.domain.value.Comparison;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.CompareIndicatorConfiguration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static java.lang.Math.abs;
import static java.lang.Math.signum;

@Scope("prototype")
@Component
public class CompareIndicatorCalculator extends AbstractIndicatorCalculator
        <CompareIndicatorConfiguration, CompareIndicatorValue> {

    private String value;
    private String compareTo;
    @Override
    public void initialize(CompareIndicatorConfiguration configuration) {
        this.value = configuration.getValue();
        this.compareTo = configuration.getCompareTo();
    }

    @Override
    public Map<ZonedDateTime, CompareIndicatorValue> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        Map<ZonedDateTime, CompareIndicatorValue> result = new TreeMap<>();

        IndicatorValueRegistry<SimpleIndicatorValue<Double>> valuesRegistry = prepareRegistry(value, symbol, period, from, to);
        SortedMap<Long, SimpleIndicatorValue<Double>> values = valuesRegistry.getCachedValues();

        IndicatorValueRegistry<SimpleIndicatorValue<Double>> compareToRegistry = prepareRegistry(compareTo, symbol, period, from, to);
        SortedMap<Long, SimpleIndicatorValue<Double>> compareToValues = compareToRegistry.getCachedValues();

        Double lastDifferencePercent = null;

        for (Long time : values.keySet()) {
            SimpleIndicatorValue<Double> value = values.get(time);
            SimpleIndicatorValue<Double> compareToValue = compareToValues.get(time);
            if (value == null
                    || value.getValue() == null
                    || compareToValue == null
                    || compareToValue.getValue() == null) {
                CompareIndicatorValue empty = createEmptyValue();
                empty.setValue(value);
                empty.setCompareToValue(compareToValue);
                result.put(Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()), empty);
                continue;
            }

            Comparison comparison = compare(value.getValue(), compareToValue.getValue());

            double differencePercent = (value.getValue() - compareToValue.getValue())*100/value.getValue();
            if (lastDifferencePercent == null) {
                lastDifferencePercent = differencePercent;
            }
            CompareState state = state(abs(differencePercent), abs(lastDifferencePercent));
            boolean hasCrossed = signum(differencePercent) != signum(lastDifferencePercent);
            lastDifferencePercent = differencePercent;

            CompareIndicatorValue compare = CompareIndicatorValue.compare(value, compareToValue, comparison, state, differencePercent, hasCrossed);

            result.put(Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()), compare);
        }
        return result;
    }

    private CompareState state(Double difference, Double lastDifference) {
        if (difference > lastDifference) {
            return CompareState.RISING;
        }
        if (difference < lastDifference) {
            return CompareState.FALLING;
        }
        return CompareState.FLAT;
    }

    private Comparison compare(Double value, Double compareToValue) {
        if (value > compareToValue) {
            return Comparison.HIGHER;
        }
        if (value < compareToValue) {
            return Comparison.LOWER;
        }
        return Comparison.EQUAL;
    }

    @Override
    protected CompareIndicatorValue createEmptyValue() {
        return CompareIndicatorValue.compare(null, null, Comparison.UNKNOWN, CompareState.FLAT, 0.0, false);
    }
}
