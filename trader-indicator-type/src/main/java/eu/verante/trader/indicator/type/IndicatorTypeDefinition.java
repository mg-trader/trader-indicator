package eu.verante.trader.indicator.type;

import eu.verante.trader.indicator.calculator.IndicatorCalculator;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;
import eu.verante.trader.indicator.model.item.IndicatorValueTypeDefinition;
import lombok.Value;

import java.util.List;

@Value
public class IndicatorTypeDefinition<C extends IndicatorConfiguration,
        V extends IndicatorValue,
        CA extends IndicatorCalculator<V>> {
    String type;

    IndicatorCategoryDefinition category;
    String description;
    IndicatorValueTypeDefinition valueType;
    Class<C> configurationType;
    List<String> configurationKeys;
    Class<CA> calculatorType;

}
