package eu.verante.trader.indicator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.domain.value.IndicatorValue;

import java.time.ZonedDateTime;

public interface IndicatorValueRegistryFactory {
    <V extends IndicatorValue> IndicatorValueRegistry<V> prepareRegistry(String indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to);
}
