package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.SimpleIndicatorConfiguration;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

@Scope("prototype")
@Component
public class SimpleIndicatorCalculator extends AbstractIndicatorCalculator
        <SimpleIndicatorConfiguration, SimpleIndicatorValue<Double>> {

    private Function<TradingCandle, BigDecimal> valueGetter;

    @Override
    public void initialize(SimpleIndicatorConfiguration configuration) {
        this.valueGetter = getValueGetter(configuration.getValue());
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);

        for (TradingCandle tradingCandle : candlesRegistry) {
            values.put(tradingCandle.time(),
                    SimpleIndicatorValue.simple(
                            valueGetter.apply(tradingCandle).doubleValue()));
        }
        return values;
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }
}
