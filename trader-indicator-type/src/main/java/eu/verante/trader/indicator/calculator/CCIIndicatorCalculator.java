package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import eu.verante.trader.indicator.model.configuration.SimplePeriodsConfiguration;
import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;

@Scope("prototype")
@Component
public class CCIIndicatorCalculator extends AbstractIndicatorCalculator<SimplePeriodsConfiguration, SimpleIndicatorValue<Double>> {

    private int periods;

    private SMAIndicatorCalculator smaIndicatorCalculator;

    @Override
    public void initialize(SimplePeriodsConfiguration configuration) {
        this.periods = configuration.getPeriods();
        this.smaIndicatorCalculator = new SMAIndicatorCalculator();
        this.smaIndicatorCalculator.initialize(new MAIndicatorConfiguration(periods, 0, SimpleValueTypeDefinition.TYPICAL));
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> averages = smaIndicatorCalculator.doCalculate(candlesRegistry);

        CircularFifoQueue<Double> typicalsQueue = new CircularFifoQueue<>(periods);

        for (TradingCandle tradingCandle : candlesRegistry) {
            double currentTypical = typical(tradingCandle).doubleValue();
            typicalsQueue.add(currentTypical);

            double currentMA = averages.get(tradingCandle.time()).getValue();
            double denominator = currentTypical - currentMA;

            double divider = typicalsQueue.stream()
                    .mapToDouble(Double::doubleValue)
                    .map(v -> Math.abs(v - currentMA))
                    .sum();
            if (divider != 0.0) {
                divider = divider * 0.015 / typicalsQueue.size();
                values.put(tradingCandle.time(), SimpleIndicatorValue.simple(denominator / divider));
            } else {
                values.put(tradingCandle.time(), SimpleIndicatorValue.simple(0.0d));
            }
        }
        return values;
    }
}
