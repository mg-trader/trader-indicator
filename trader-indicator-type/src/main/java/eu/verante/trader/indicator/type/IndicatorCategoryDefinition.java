package eu.verante.trader.indicator.type;

public enum IndicatorCategoryDefinition {
    SIMPLE,
    MOVING_AVERAGE,
    OSCILLATOR,
    COMPARE,
    TREND

}
