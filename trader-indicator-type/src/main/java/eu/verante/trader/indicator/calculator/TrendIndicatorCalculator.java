package eu.verante.trader.indicator.calculator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.IndicatorValueRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.domain.value.TrendIndicatorValue;
import eu.verante.trader.indicator.domain.value.TrendType;
import eu.verante.trader.indicator.model.configuration.TrendIndicatorConfiguration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import static eu.verante.trader.indicator.domain.value.SimpleIndicatorValue.simple;
import static eu.verante.trader.indicator.domain.value.TrendIndicatorValue.trend;
import static eu.verante.trader.indicator.domain.value.TrendType.UNKNOWN;

@Scope("prototype")
@Component
public class TrendIndicatorCalculator extends AbstractIndicatorCalculator
        <TrendIndicatorConfiguration, TrendIndicatorValue> {

    private TrendIndicatorConfiguration configuration;
    @Override
    public void initialize(TrendIndicatorConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Map<ZonedDateTime, TrendIndicatorValue> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        Map<ZonedDateTime, TrendIndicatorValue> values = new TreeMap<>();
        IndicatorValueRegistry<SimpleIndicatorValue<Double>> valuesRegistry
                = prepareRegistry(configuration.getIndicator(), symbol, period, from, to);

        TrendType trend = UNKNOWN;
        SimpleIndicatorValue<Double> lastApex = null;
        SimpleIndicatorValue<Double> apexCandidate = null;

        for (Map.Entry<Long, SimpleIndicatorValue<Double>> entry : valuesRegistry.getCachedValues().entrySet()) {
            SimpleIndicatorValue<Double> value = entry.getValue();
            if (value == null || value.getValue() == null) {
                TrendIndicatorValue emptyValue = createEmptyValue();
                emptyValue.setLastApex(lastApex);
                values.put(
                        Instant.ofEpochMilli(entry.getKey()).atZone(ZoneId.systemDefault()),
                        emptyValue);
                continue;
            }
            if (lastApex == null || lastApex.getValue() == null) {
                 lastApex = value;
                 apexCandidate = lastApex;
            }
            TrendType newTrend = calculateNewTrend(apexCandidate, value.getValue(), trend);
            if (newTrend != trend) {
                lastApex = apexCandidate;
            }
            trend = newTrend;
            apexCandidate = calculateApexCandidate(apexCandidate, value, newTrend);

            double valueDifference = Math.abs(lastApex.getValue() - value.getValue());
            double percentDifference = valueDifference*100.0/lastApex.getValue();
            values.put(
                    Instant.ofEpochMilli(entry.getKey()).atZone(ZoneId.systemDefault()),
                    trend(trend, value, lastApex, percentDifference, valueDifference));
        }
        return values;

    }

    private TrendType calculateNewTrend(SimpleIndicatorValue<Double> apexCandidate, double value, TrendType currentTrend) {
        final Double thresholdPercent = configuration.getThreshold();
        double thresholdMultiplier = thresholdPercent/100.0;
        double thresholdedValue = switch (currentTrend) {
            case RISING -> apexCandidate.getValue() - apexCandidate.getValue() * thresholdMultiplier;
            case FALLING -> apexCandidate.getValue() + apexCandidate.getValue() * thresholdMultiplier;
            case UNKNOWN -> Optional.ofNullable(apexCandidate.getValue()).orElse(value);
        };
        if (thresholdedValue > value) {
            return TrendType.FALLING;
        } else if (thresholdedValue < value) {
            return TrendType.RISING;
        }
        return currentTrend;
    }

    private SimpleIndicatorValue<Double> calculateApexCandidate(SimpleIndicatorValue<Double> currentApex, SimpleIndicatorValue<Double> indicatorValue, TrendType trendType) {
        return switch (trendType) {
            case UNKNOWN -> currentApex;
            case RISING -> simple(Math.max(currentApex.getValue(), indicatorValue.getValue()));
            case FALLING -> simple(Math.min(currentApex.getValue(), indicatorValue.getValue()));
        };
    }

    @Override
    protected TrendIndicatorValue createEmptyValue() {
        return TrendIndicatorValue.trend(UNKNOWN, null, null, 0.0, 0.0);
    }
}
