package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import eu.verante.trader.stock.candle.TradingCandle;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Scope("prototype")
@Component
public class WMAIndicatorCalculator extends AbstractIndicatorCalculator
        <MAIndicatorConfiguration, SimpleIndicatorValue<Double>> {
    private int periods;
    private int shift;
    private SimpleValueTypeDefinition base;

    @Override
    public void initialize(MAIndicatorConfiguration configuration) {
        this.periods = configuration.getPeriods();
        this.shift = configuration.getShift();
        this.base = configuration.getBase();
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);
        return doCalculate(candlesRegistry);
    }

    Map<ZonedDateTime, SimpleIndicatorValue<Double>> doCalculate(CandlesRegistry candlesRegistry) {
        Function<TradingCandle, BigDecimal> valueGetter = getValueGetter(base);
        Map<ZonedDateTime, Double> values = StreamSupport.stream(
                        Spliterators.spliteratorUnknownSize(candlesRegistry.iterator(), Spliterator.ORDERED), false)
                .collect(Collectors.toMap(
                        candle -> candle.time(),
                        candle -> valueGetter.apply(candle).doubleValue()
                ));
        return doCalculate(values);
    }

    Map<ZonedDateTime, SimpleIndicatorValue<Double>> doCalculate(Map<ZonedDateTime, Double> inputValues) {
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();
        CircularFifoQueue<Double> candlesQueue = new CircularFifoQueue<>(periods);
        for (Map.Entry<ZonedDateTime, Double> value : inputValues.entrySet()) {
            candlesQueue.add(value.getValue());
            double weightedSum = 0.0;
            for (int i = 0; i < candlesQueue.size(); i++) {
                weightedSum += candlesQueue.get(i) * (i+1);
            }

            int weightSum = candlesQueue.size()* (candlesQueue.size()+1) /2;
            values.put(value.getKey(), SimpleIndicatorValue.simple(weightedSum/weightSum));
        }
        return shift(values, shift);
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }
}
