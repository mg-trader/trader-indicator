package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.SimplePeriodsConfiguration;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.TreeMap;

@Scope("prototype")
@Component
public class RSIIndicatorCalculator extends AbstractIndicatorCalculator<SimplePeriodsConfiguration, SimpleIndicatorValue<Double>>{

    private int periods;
    @Override
    public void initialize(SimplePeriodsConfiguration configuration) {
        this.periods = configuration.getPeriods();
    }

    @Override
    protected SimpleIndicatorValue<Double> createEmptyValue() {
        return SimpleIndicatorValue.simple(null);
    }

    @Override
    public Map<ZonedDateTime, SimpleIndicatorValue<Double>> calculate(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        CandlesRegistry candlesRegistry = prepareRegistry(symbol, period, from, to);
        Map<ZonedDateTime, SimpleIndicatorValue<Double>> values = new TreeMap<>();

        CircularFifoQueue<Double> changesQueue = new CircularFifoQueue<>(periods);
        Double lastClose = null;

        for (TradingCandle tradingCandle : candlesRegistry) {
            if (lastClose == null) {
                lastClose = tradingCandle.close().doubleValue();
            }
            changesQueue.add(tradingCandle.close().doubleValue() - lastClose);
            lastClose = tradingCandle.close().doubleValue();
            if (changesQueue.size() < periods) {
                values.put(tradingCandle.time(), createEmptyValue());
                continue;
            }

            double averageRises = changesQueue.stream()
                    .filter(v -> v > 0)
                    .mapToDouble(v -> v)
                    .average()
                    .orElse(0.0d);
            double averageDrops = changesQueue.stream()
                    .filter(v -> v < 0)
                    .mapToDouble(v -> -v)
                    .average()
                    .orElse(0.0d);
            double relativeStrength = 100;
            if (averageDrops != 0) {
                relativeStrength = averageRises/averageDrops;
            }
            double rsIndex = 100 - (100/(1+relativeStrength));
            values.put(tradingCandle.time(), SimpleIndicatorValue.simple(rsIndex));
        }

        return values;
    }
}
