package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.CandlesRegistryFactory;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.SimplePeriodsConfiguration;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static eu.verante.trader.indicator.domain.value.SimpleIndicatorValue.simple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class RSIIndicatorCalculatorTest {

    @Mock
    private CandlesRegistryFactory registryFactory;

    @InjectMocks
    private RSIIndicatorCalculator calculator = new RSIIndicatorCalculator();

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testRSICalculation() {
        SimplePeriodsConfiguration configuration = new SimplePeriodsConfiguration(4);
        calculator.initialize(configuration);

        ZonedDateTime start = ZonedDateTime.of(2024, 4, 25, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime end = start.plusDays(10);
        when(registryFactory.prepareRegistry("symbol", Period.DAY, start, end)).thenReturn(
                prepareCandles("symbol", start));

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> result = calculator.calculate("symbol", Period.DAY, start, end);

        assertEquals(result.size(), 11);
        assertEquals(result.get(start), simple(null));
        assertEquals(result.get(start.plusDays(1)), simple(null));
        assertEquals(result.get(start.plusDays(2)), simple(null));
        assertEquals(result.get(start.plusDays(3)), simple(50.0));
        assertEquals(result.get(start.plusDays(4)), simple(57.14285714285714));
        assertEquals(result.get(start.plusDays(5)), simple(57.14285714285714));
        assertEquals(result.get(start.plusDays(6)), simple(80.0));
        assertEquals(result.get(start.plusDays(7)), simple(77.77777777777777));
        assertEquals(result.get(start.plusDays(8)), simple(99.00990099009901));
        assertEquals(result.get(start.plusDays(9)), simple(30.434782608695656));
        assertEquals(result.get(start.plusDays(10)), simple(18.181818181818187));
    }

    private CandlesRegistry prepareCandles(String symbol, ZonedDateTime dateStart) {
        List<TradingCandle> candles = new ArrayList<>();
        candles.add(candle(dateStart.plusDays(0),102.0));
        candles.add(candle(dateStart.plusDays(1),102.0));
        candles.add(candle(dateStart.plusDays(2),104.0));
        candles.add(candle(dateStart.plusDays(3),102.0));
        candles.add(candle(dateStart.plusDays(4),101.0));
        candles.add(candle(dateStart.plusDays(5),101.0));
        candles.add(candle(dateStart.plusDays(6),107.0));
        candles.add(candle(dateStart.plusDays(7),108.0));
        candles.add(candle(dateStart.plusDays(8),108.0));
        candles.add(candle(dateStart.plusDays(9),100.0));
        candles.add(candle(dateStart.plusDays(10),99.0));

        return new CandlesRegistry(candles);
    }

    private TradingCandle candle(ZonedDateTime time, double close) {
        return TradingCandle.builder()
                .time(time)
                .close(BigDecimal.valueOf(close))
                .build();
    }
}