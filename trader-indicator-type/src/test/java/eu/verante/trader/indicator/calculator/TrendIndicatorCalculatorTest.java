package eu.verante.trader.indicator.calculator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.CandlesRegistryFactory;
import eu.verante.trader.indicator.IndicatorValueRegistry;
import eu.verante.trader.indicator.IndicatorValueRegistryFactory;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.domain.value.TrendIndicatorValue;
import eu.verante.trader.indicator.domain.value.TrendType;
import eu.verante.trader.indicator.model.configuration.TrendIndicatorConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static eu.verante.trader.indicator.domain.value.SimpleIndicatorValue.simple;
import static eu.verante.trader.indicator.domain.value.TrendIndicatorValue.trend;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class TrendIndicatorCalculatorTest {

    private static final long dateMillis = 86400000;

    @Mock
    private CandlesRegistryFactory candlesRegistryFactory;

    @Mock
    private IndicatorValueRegistryFactory indicatorValueRegistryFactory;

    @InjectMocks
    private TrendIndicatorCalculator calculator = new TrendIndicatorCalculator();

    @Test
    void testIndicatorCalculation() {
        MockitoAnnotations.openMocks(this);

        TrendIndicatorConfiguration configuration = new TrendIndicatorConfiguration();
        configuration.setIndicator("test");
        configuration.setThreshold(0.3);
        calculator.initialize(configuration);

        ZonedDateTime dateStart = ZonedDateTime.of(2023, 3, 1, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime dateEnd = dateStart.plusDays(30);
        SortedMap<Long, SimpleIndicatorValue<Double>> data = prepareData(dateStart);
        when(indicatorValueRegistryFactory.prepareRegistry("test", "symbol", Period.DAY, dateStart, dateEnd))
                .thenReturn(new IndicatorValueRegistry(data));

        Map<ZonedDateTime, TrendIndicatorValue> result = calculator.calculate("symbol", Period.DAY, dateStart, dateEnd);
        assertEquals(trend(TrendType.UNKNOWN, getData(data, 0), getData(data, 0), 0.0, 0.0), result.get(dateStart.plusDays(0)));
        assertEquals(trend(TrendType.RISING, getData(data, 1), getData(data, 0), 0.1, 1.0), result.get(dateStart.plusDays(1)));
        assertEquals(trend(TrendType.RISING, getData(data, 2), getData(data, 0), 0.2, 2.0), result.get(dateStart.plusDays(2)));
        assertEquals(trend(TrendType.RISING, getData(data, 3), getData(data, 0), 0.3, 3.0), result.get(dateStart.plusDays(3)));
        assertEquals(trend(TrendType.RISING, getData(data, 4), getData(data, 0), 0.4, 4.0), result.get(dateStart.plusDays(4)));
        assertEquals(trend(TrendType.RISING, getData(data, 5), getData(data, 0), 0.5, 5.0), result.get(dateStart.plusDays(5)));

        assertEquals(trend(TrendType.RISING, getData(data, 6), getData(data, 0), 0.3, 3.0), result.get(dateStart.plusDays(6)));
        assertEquals(trend(TrendType.RISING, getData(data, 7), getData(data, 0), 0.5, 5.0), result.get(dateStart.plusDays(7)));
        assertEquals(trend(TrendType.RISING, getData(data, 8), getData(data, 0), 0.6, 6.0), result.get(dateStart.plusDays(8)));
        assertEquals(trend(TrendType.RISING, getData(data, 9), getData(data, 0), 0.4, 4.0), result.get(dateStart.plusDays(9)));
        assertEquals(trend(TrendType.RISING, getData(data, 10), getData(data, 0), (1003.1-1000.0)/10, 1003.1-1000.0), result.get(dateStart.plusDays(10)));
        assertEquals(trend(TrendType.RISING, getData(data, 11), getData(data, 0), 0.3, 3.0), result.get(dateStart.plusDays(11)));

        assertEquals(trend(TrendType.FALLING, getData(data, 12), getData(data, 8), (1006.0-1002.9)*100/1006.0, 1006.0-1002.9), result.get(dateStart.plusDays(12)));
        assertEquals(trend(TrendType.FALLING, getData(data, 13), getData(data, 8), 400.0/1006.0, 4.0), result.get(dateStart.plusDays(13)));
        assertEquals(trend(TrendType.FALLING, getData(data, 14), getData(data, 8), 200.0/1006.0, 2.0), result.get(dateStart.plusDays(14)));

        assertEquals(trend(TrendType.RISING, getData(data, 15), getData(data, 13), (1005.1-1002.0)*100/1002.0, 1005.1-1002.0), result.get(dateStart.plusDays(15)));
    }

    private static SimpleIndicatorValue<Double> getData(SortedMap<Long, SimpleIndicatorValue<Double>> data, int day) {
        Long startDate = data.keySet().iterator().next();
        return data.get(startDate + day * dateMillis);
    }

    private SortedMap<Long, SimpleIndicatorValue<Double>> prepareData(ZonedDateTime dateStart) {
        SortedMap<Long, SimpleIndicatorValue<Double>> result = new TreeMap<>();
        result.put(dateStart.plusDays(0).toInstant().toEpochMilli(), simple(1000.0));
        result.put(dateStart.plusDays(1).toInstant().toEpochMilli(), simple(1001.0));
        result.put(dateStart.plusDays(2).toInstant().toEpochMilli(), simple(1002.0));
        result.put(dateStart.plusDays(3).toInstant().toEpochMilli(), simple(1003.0));
        result.put(dateStart.plusDays(4).toInstant().toEpochMilli(), simple(1004.0));
        result.put(dateStart.plusDays(5).toInstant().toEpochMilli(), simple(1005.0));

        result.put(dateStart.plusDays(6).toInstant().toEpochMilli(), simple(1003.0));
        result.put(dateStart.plusDays(7).toInstant().toEpochMilli(), simple(1005.0));
        result.put(dateStart.plusDays(8).toInstant().toEpochMilli(), simple(1006.0));
        result.put(dateStart.plusDays(9).toInstant().toEpochMilli(), simple(1004.0));
        result.put(dateStart.plusDays(10).toInstant().toEpochMilli(), simple(1003.1));


        result.put(dateStart.plusDays(11).toInstant().toEpochMilli(), simple(1003.0));
        result.put(dateStart.plusDays(12).toInstant().toEpochMilli(), simple(1002.9));
        result.put(dateStart.plusDays(13).toInstant().toEpochMilli(), simple(1002.0));
        result.put(dateStart.plusDays(14).toInstant().toEpochMilli(), simple(1004.0));
        result.put(dateStart.plusDays(15).toInstant().toEpochMilli(), simple(1005.1));
        return result;


    }

}