package eu.verante.trader.indicator.calculator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.CandlesRegistryFactory;
import eu.verante.trader.indicator.IndicatorValueRegistry;
import eu.verante.trader.indicator.IndicatorValueRegistryFactory;
import eu.verante.trader.indicator.domain.value.CompareIndicatorValue;
import eu.verante.trader.indicator.domain.value.CompareState;
import eu.verante.trader.indicator.domain.value.Comparison;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.CompareIndicatorConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static eu.verante.trader.indicator.domain.value.CompareIndicatorValue.compare;
import static eu.verante.trader.indicator.domain.value.SimpleIndicatorValue.simple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CompareIndicatorCalculatorTest {

    private static final long dateMillis = 86400000;

    @Mock
    private CandlesRegistryFactory candlesRegistryFactory;

    @Mock
    private IndicatorValueRegistryFactory indicatorValueRegistryFactory;

    @InjectMocks
    private CompareIndicatorCalculator calculator = new CompareIndicatorCalculator();

    @Test
    void testIndicatorCalculation() {
        MockitoAnnotations.openMocks(this);

        CompareIndicatorConfiguration configuration = new CompareIndicatorConfiguration();
        configuration.setValue("value");
        configuration.setCompareTo("compareTo");
        calculator.initialize(configuration);

        ZonedDateTime dateStart = ZonedDateTime.of(2023, 3, 1, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime dateEnd = dateStart.plusDays(30);
        SortedMap<Long, SimpleIndicatorValue<Double>> valueData = prepareValueData(dateStart);
        when(indicatorValueRegistryFactory.prepareRegistry("value", "symbol", Period.DAY, dateStart, dateEnd))
                .thenReturn(new IndicatorValueRegistry(valueData));

        SortedMap<Long, SimpleIndicatorValue<Double>> compareToData = prepareCompareToData(dateStart);
        when(indicatorValueRegistryFactory.prepareRegistry("compareTo", "symbol", Period.DAY, dateStart, dateEnd))
                .thenReturn(new IndicatorValueRegistry(compareToData));

        Map<ZonedDateTime, CompareIndicatorValue> result = calculator.calculate("symbol", Period.DAY, dateStart, dateEnd);

        assertEquals(compare(getData(valueData, 0), getData(compareToData, 0), Comparison.HIGHER, CompareState.FLAT, (102.0-100.0)*100.0/102.0, false), result.get(dateStart.plusDays(0)));
        assertEquals(compare(getData(valueData, 1), getData(compareToData, 1), Comparison.HIGHER, CompareState.FALLING, (102.0-101.0)*100.0/102.0, false), result.get(dateStart.plusDays(1)));
        assertEquals(compare(getData(valueData, 2), getData(compareToData, 2), Comparison.HIGHER, CompareState.RISING, (103.0-99.0)*100.0/103.0, false), result.get(dateStart.plusDays(2)));

        assertEquals(compare(getData(valueData, 3), getData(compareToData, 3), Comparison.LOWER, CompareState.FALLING, (99.0-100.0)*100.0/99.0, true), result.get(dateStart.plusDays(3)));
        assertEquals(compare(getData(valueData, 4), getData(compareToData, 4), Comparison.LOWER, CompareState.RISING, (98.0-100.0)*100.0/98.0, false), result.get(dateStart.plusDays(4)));
        assertEquals(compare(getData(valueData, 5), getData(compareToData, 5), Comparison.LOWER, CompareState.FALLING, (99.0-100.0)*100.0/99.0, false), result.get(dateStart.plusDays(5)));


        assertEquals(compare(getData(valueData, 6), getData(compareToData, 6), Comparison.EQUAL, CompareState.FALLING, (100.0-100.0)*100.0/100.0, true), result.get(dateStart.plusDays(6)));
        assertEquals(compare(getData(valueData, 7), getData(compareToData, 7), Comparison.EQUAL, CompareState.FLAT, (100.0-100.0)*100.0/100.0, false), result.get(dateStart.plusDays(7)));
        assertEquals(compare(getData(valueData, 8), getData(compareToData, 8), Comparison.HIGHER, CompareState.RISING, (101.0-100.0)*100.0/101.0, true), result.get(dateStart.plusDays(8)));
    }

    private SortedMap<Long, SimpleIndicatorValue<Double>> prepareValueData(ZonedDateTime dateStart) {
        SortedMap<Long, SimpleIndicatorValue<Double>> result = new TreeMap<>();

        result.put(dateStart.plusDays(0).toInstant().toEpochMilli(), simple(102.0));
        result.put(dateStart.plusDays(1).toInstant().toEpochMilli(), simple(102.0));
        result.put(dateStart.plusDays(2).toInstant().toEpochMilli(), simple(103.0));

        result.put(dateStart.plusDays(3).toInstant().toEpochMilli(), simple(99.0));
        result.put(dateStart.plusDays(4).toInstant().toEpochMilli(), simple(98.0));
        result.put(dateStart.plusDays(5).toInstant().toEpochMilli(), simple(99.0));

        result.put(dateStart.plusDays(6).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(7).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(8).toInstant().toEpochMilli(), simple(101.0));

        return result;
    }

    private SortedMap<Long, SimpleIndicatorValue<Double>> prepareCompareToData(ZonedDateTime dateStart) {
        SortedMap<Long, SimpleIndicatorValue<Double>> result = new TreeMap<>();

        result.put(dateStart.plusDays(0).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(1).toInstant().toEpochMilli(), simple(101.0));
        result.put(dateStart.plusDays(2).toInstant().toEpochMilli(), simple(99.0));

        result.put(dateStart.plusDays(3).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(4).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(5).toInstant().toEpochMilli(), simple(100.0));

        result.put(dateStart.plusDays(6).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(7).toInstant().toEpochMilli(), simple(100.0));
        result.put(dateStart.plusDays(8).toInstant().toEpochMilli(), simple(100.0));

        return result;
    }

    private static SimpleIndicatorValue<Double> getData(SortedMap<Long, SimpleIndicatorValue<Double>> data, int day) {
        Long startDate = data.keySet().iterator().next();
        return data.get(startDate + day * dateMillis);
    }

}