package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.CandlesRegistryFactory;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.SimplePeriodsConfiguration;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static eu.verante.trader.indicator.domain.value.SimpleIndicatorValue.simple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CCIIndicatorCalculatorTest {

    @Mock
    private CandlesRegistryFactory registryFactory;

    @InjectMocks
    private CCIIndicatorCalculator calculator = new CCIIndicatorCalculator();

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCCICalculation() {
        SimplePeriodsConfiguration configuration = new SimplePeriodsConfiguration(5);
        calculator.initialize(configuration);

        ZonedDateTime start = ZonedDateTime.of(2024, 4, 25, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime end = start.plusDays(10);
        when(registryFactory.prepareRegistry("symbol", Period.DAY, start, end)).thenReturn(
                prepareCandles("symbol", start, end));

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> result = calculator.calculate("symbol", Period.DAY, start, end);

        assertEquals(result.size(), 11);
        assertEquals(result.get(start), simple(0.0d));
        assertEquals(result.get(start.plusDays(1)), simple(66.66666666666667));
        assertEquals(result.get(start.plusDays(2)), simple(100.00000000000001));
        assertEquals(result.get(start.plusDays(3)), simple(133.33333333333337));
        assertEquals(result.get(start.plusDays(4)), simple(150.88636648650146));
        assertEquals(result.get(start.plusDays(5)), simple(150.88625491527057));
    }

    private CandlesRegistry prepareCandles(String symbol, ZonedDateTime start, ZonedDateTime end) {
//        Random random = new Random(System.currentTimeMillis());
        ZonedDateTime currentDate = start;
        BigDecimal value = BigDecimal.ONE;
//        BigDecimal value = BigDecimal.valueOf(random.nextDouble(200.0d, 400.0d));
        List<TradingCandle> candles = new ArrayList<>();
        while (!currentDate.isAfter(end)) {
            candles.add(TradingCandle.builder()
                    .symbol(symbol)
                    .period(Period.DAY)
                    .time(currentDate)
                    .close(value)
                    .low(value.multiply(BigDecimal.valueOf(0.9)))
                    .high(value.multiply(BigDecimal.valueOf(1.2)))
                    .build());
            value = value.multiply(BigDecimal.valueOf(2.5d));
//            value = value = BigDecimal.valueOf(random.nextDouble(200.0d, 400.0d));
            currentDate = currentDate.plusDays(1);
        }

        return new CandlesRegistry(candles);
    }

}