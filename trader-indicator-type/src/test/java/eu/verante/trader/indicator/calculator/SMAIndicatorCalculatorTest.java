package eu.verante.trader.indicator.calculator;

import eu.verante.trader.indicator.CandlesRegistry;
import eu.verante.trader.indicator.CandlesRegistryFactory;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static eu.verante.trader.indicator.domain.value.SimpleIndicatorValue.simple;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class SMAIndicatorCalculatorTest {

    @Mock
    private CandlesRegistryFactory registryFactory;

    @InjectMocks
    private SMAIndicatorCalculator calculator = new SMAIndicatorCalculator();

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testSMACalculation() {
        MAIndicatorConfiguration configuration = new MAIndicatorConfiguration(3 ,0, SimpleValueTypeDefinition.CLOSE);
        calculator.initialize(configuration);

        ZonedDateTime start = ZonedDateTime.of(2024, 4, 25, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime end = start.plusDays(4);
        when(registryFactory.prepareRegistry("symbol", Period.DAY, start, end)).thenReturn(
                prepareCandles("symbol", start, end));

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> result = calculator.calculate("symbol", Period.DAY, start, end);

        assertEquals(result.size(), 5);
        assertEquals(result.get(start), simple(1.0d));
        assertEquals(result.get(start.plusDays(1)), simple(1.5d));
        assertEquals(result.get(start.plusDays(2)), simple(7.0d/3.0d));
        assertEquals(result.get(start.plusDays(3)), simple(14.0d/3.0d));
        assertEquals(result.get(start.plusDays(4)), simple(28.0d/3.0d));
    }

    @Test
    void testSMAPositiveShift() {
        MAIndicatorConfiguration configuration = new MAIndicatorConfiguration(3 ,2, SimpleValueTypeDefinition.CLOSE);
        calculator.initialize(configuration);

        ZonedDateTime start = ZonedDateTime.of(2024, 4, 25, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime end = start.plusDays(4);
        when(registryFactory.prepareRegistry("symbol", Period.DAY, start, end)).thenReturn(
                prepareCandles("symbol", start, end));

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> result = calculator.calculate("symbol", Period.DAY, start, end);

        assertEquals(result.size(), 5);
        assertEquals(result.get(start), simple(null));
        assertEquals(result.get(start.plusDays(1)), simple(null));
        assertEquals(result.get(start.plusDays(2)), simple(1.0d));
        assertEquals(result.get(start.plusDays(3)), simple(1.5d));
        assertEquals(result.get(start.plusDays(4)), simple(7.0d/3.0d));
    }

    @Test
    void testSMANegativeShift() {
        MAIndicatorConfiguration configuration = new MAIndicatorConfiguration(3 ,-2, SimpleValueTypeDefinition.CLOSE);
        calculator.initialize(configuration);

        ZonedDateTime start = ZonedDateTime.of(2024, 4, 25, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime end = start.plusDays(4);
        when(registryFactory.prepareRegistry("symbol", Period.DAY, start, end)).thenReturn(
                prepareCandles("symbol", start, end));

        Map<ZonedDateTime, SimpleIndicatorValue<Double>> result = calculator.calculate("symbol", Period.DAY, start, end);

        assertEquals(result.size(), 5);
        assertEquals(result.get(start), simple(7.0d/3.0d));
        assertEquals(result.get(start.plusDays(1)), simple(14.0d/3.0d));
        assertEquals(result.get(start.plusDays(2)), simple(28.0d/3.0d));
        assertEquals(result.get(start.plusDays(3)), simple(null));
        assertEquals(result.get(start.plusDays(4)), simple(null));
    }

    private CandlesRegistry prepareCandles(String symbol, ZonedDateTime start, ZonedDateTime end) {
        ZonedDateTime currentDate = start;
        double value = 1.0;
        List<TradingCandle> candles = new ArrayList<>();
        while (!currentDate.isAfter(end)) {
            candles.add(TradingCandle.builder()
                    .symbol(symbol)
                    .period(Period.DAY)
                    .time(currentDate)
                    .close(BigDecimal.valueOf(value))
                    .build());
            value *=2.0;
            currentDate = currentDate.plusDays(1);
        }

        return new CandlesRegistry(candles);
    }



}