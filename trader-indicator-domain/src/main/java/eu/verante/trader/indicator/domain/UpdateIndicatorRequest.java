package eu.verante.trader.indicator.domain;

import lombok.Data;

import java.util.Map;

@Data
public class UpdateIndicatorRequest {

    private String description;

    private Map<String, Object> configuration;
}
