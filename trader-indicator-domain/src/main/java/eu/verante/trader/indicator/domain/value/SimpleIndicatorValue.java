package eu.verante.trader.indicator.domain.value;

import lombok.Data;

@Data
public class SimpleIndicatorValue<V> implements IndicatorValue {

    V value;

    public static <V> SimpleIndicatorValue<V> simple(V value) {
        SimpleIndicatorValue<V> simple = new SimpleIndicatorValue<>();
        simple.setValue(value);
        return simple;
    }
}
