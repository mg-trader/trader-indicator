package eu.verante.trader.indicator.domain;

import com.fasterxml.jackson.annotation.JsonValue;
import eu.verante.trader.indicator.domain.value.CompareIndicatorValue;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.domain.value.SimpleIndicatorValue;
import eu.verante.trader.indicator.domain.value.TrendIndicatorValue;

public enum IndicatorValueType {

    SIMPLE("simple", SimpleIndicatorValue.class),
    TREND("trend", TrendIndicatorValue.class),
    COMPARE("compare", CompareIndicatorValue.class);

    private final String value;
    private final Class<? extends IndicatorValue> valueClass;


    IndicatorValueType(String value, Class<? extends IndicatorValue> valueClass) {
        this.value = value;
        this.valueClass = valueClass;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public Class<? extends IndicatorValue> getValueClass() {
        return valueClass;
    }
}
