package eu.verante.trader.indicator.domain;

import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.IndicatorType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;

public interface IndicatorTypeResource {
    @GetMapping("")
    Collection<IndicatorType> getIndicators(@RequestParam(name = "category", required = false) IndicatorCategory category);

    @GetMapping("/{type}")
    IndicatorType getIndicatorTypeByName(@PathVariable("type") String type);
}
