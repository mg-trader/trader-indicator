package eu.verante.trader.indicator.domain.value;

public enum Comparison {
    HIGHER,
    EQUAL,
    LOWER,
    UNKNOWN,
}
