package eu.verante.trader.indicator.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum IndicatorCategory {
    SIMPLE("Simple"),
    MOVING_AVERAGE("MovingAverage"),

    OSCILLATOR("Oscillator"),
    COMPARE("Compare"),

    TREND("Trend");

    private final String value;

    IndicatorCategory(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
