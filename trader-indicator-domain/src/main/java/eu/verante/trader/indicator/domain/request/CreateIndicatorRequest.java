package eu.verante.trader.indicator.domain.request;

import lombok.Data;

import java.util.Map;

@Data
public class CreateIndicatorRequest {

    private String name;

    private String description;
    private String indicatorType;
    private Map<String, Object> configuration;
}
