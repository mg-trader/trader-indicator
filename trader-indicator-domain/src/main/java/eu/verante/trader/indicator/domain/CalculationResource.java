package eu.verante.trader.indicator.domain;

import eu.verante.trader.indicator.domain.request.CalculationRequest;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;
import java.util.UUID;

public interface CalculationResource {
    @PostMapping("/calculate")
    AsyncProcess calculate(@RequestBody CalculationRequest request);

    @GetMapping("/{processId}/status")
    AsyncProcess getProcessStatus(@PathVariable("processId") UUID processId);

    @GetMapping("/values")
    Map<Long, IndicatorValue> getCalculationResults(@SpringQueryMap CalculationRequest request);

    @PostMapping("/cleanup")
    void cleanupAllCalculations();
}
