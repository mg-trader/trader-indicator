package eu.verante.trader.indicator.domain;

import lombok.Data;

import java.util.Map;

@Data
public class Indicator {
    private String name;

    private String description;
    private String type;

    private Map<String, Object> configuration;
}
