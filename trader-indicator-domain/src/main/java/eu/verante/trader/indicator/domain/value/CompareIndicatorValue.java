package eu.verante.trader.indicator.domain.value;

import lombok.Data;

@Data
public class CompareIndicatorValue implements IndicatorValue {

    private SimpleIndicatorValue<Double> value;

    private SimpleIndicatorValue<Double> compareToValue;

    private Comparison comparison;
    private CompareState state;

    private Double differencePercent;

    private boolean hasCrossed;

    public static CompareIndicatorValue compare(SimpleIndicatorValue<Double> simpleValue, SimpleIndicatorValue<Double> compareToValue,
                                                Comparison comparison, CompareState state, double differencePercent, boolean hasCrossed) {
        CompareIndicatorValue value = new CompareIndicatorValue();
        value.setValue(simpleValue);
        value.setCompareToValue(compareToValue);
        value.setComparison(comparison);
        value.setState(state);
        value.setDifferencePercent(differencePercent);
        value.setHasCrossed(hasCrossed);
        return value;
    }

}
