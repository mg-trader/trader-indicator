package eu.verante.trader.indicator.domain.value;

public enum SimpleValueType {

    OPEN,
    CLOSE,
    HIGH,
    LOW,
    VOLUME

}
