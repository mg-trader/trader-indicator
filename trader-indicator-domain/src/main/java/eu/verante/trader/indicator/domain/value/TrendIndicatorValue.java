package eu.verante.trader.indicator.domain.value;

import lombok.Data;

@Data
public class TrendIndicatorValue implements IndicatorValue {
    private TrendType trendType;
    private SimpleIndicatorValue<Double> currentValue;
    private SimpleIndicatorValue<Double> lastApex;
    private Double percentDifferenceFromApex;
    private Double valueDifferenceFromApex;

    public static TrendIndicatorValue trend(TrendType trendType, SimpleIndicatorValue<Double> currentValue, SimpleIndicatorValue<Double> lastApex,
                                            Double percentDifferenceFromApex, Double valueDifferenceFromApex) {
        TrendIndicatorValue value = new TrendIndicatorValue();
        value.setTrendType(trendType);
        value.setCurrentValue(currentValue);
        value.setLastApex(lastApex);
        value.setPercentDifferenceFromApex(percentDifferenceFromApex);
        value.setValueDifferenceFromApex(valueDifferenceFromApex);
        return value;
    }
}
