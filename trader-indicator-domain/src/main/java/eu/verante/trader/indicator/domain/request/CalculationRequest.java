package eu.verante.trader.indicator.domain.request;

import eu.verante.trader.stock.candle.Period;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class CalculationRequest {

    private String indicator;

    private String symbol;
    private Period period;

    private ZonedDateTime from;
    private ZonedDateTime to;
}
