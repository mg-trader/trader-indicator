package eu.verante.trader.indicator.domain;

import lombok.Data;

import java.util.List;

@Data
public class IndicatorType {

    private String type;

    private IndicatorCategory category;
    private String description;

    private IndicatorValueType valueType;

    private List<String> configurationKeys;
}
