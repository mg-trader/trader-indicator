package eu.verante.trader.indicator.domain.value;

public enum CompareState {
    RISING,
    FLAT,
    FALLING
}
