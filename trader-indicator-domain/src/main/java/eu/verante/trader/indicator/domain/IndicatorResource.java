package eu.verante.trader.indicator.domain;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.UpdateIndicatorRequest;
import eu.verante.trader.indicator.domain.request.CreateIndicatorRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface IndicatorResource {
    @GetMapping("")
    List<Indicator> getAllIndicators(@RequestParam(name = "type", required = false) String type);

    @GetMapping("/{name}")
    Indicator getIndicatorByName(@PathVariable("name") String name);

    @PostMapping("")
    void createNewIndicator(@RequestBody CreateIndicatorRequest request);

    @PutMapping("/{name}")
    void updateIndicator(@PathVariable("name") String name, @RequestBody UpdateIndicatorRequest request);

    @DeleteMapping("/{name}")
    void deleteIndicator(@PathVariable("name") String name);

    @PostMapping("/cleanup")
    void cleanupIndicators();
}
