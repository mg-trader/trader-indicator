package eu.verante.trader.indicator.api;

import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.domain.IndicatorTypeResource;
import eu.verante.trader.indicator.service.IndicatorTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/type")
public class IndicatorTypeController implements IndicatorTypeResource {

    @Autowired
    private IndicatorTypeService indicatorTypeService;

    @Override
    public Collection<IndicatorType> getIndicators(IndicatorCategory category) {
        return indicatorTypeService.getAll(Optional.ofNullable(category));
    }

    @Override
    public IndicatorType getIndicatorTypeByName(String type) {
        return indicatorTypeService.getByName(type);
    }
}
