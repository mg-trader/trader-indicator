package eu.verante.trader.indicator.client;

import eu.verante.trader.domain.management.TraderManagementResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "management", url = "${clients.management.url}")
public interface TradingManagementClient extends TraderManagementResource {

}
