package eu.verante.trader.indicator.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.UpdateIndicatorRequest;
import eu.verante.trader.indicator.domain.request.CreateIndicatorRequest;
import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;
import eu.verante.trader.indicator.model.item.IndicatorItem;
import eu.verante.trader.indicator.model.repository.IndicatorRepository;
import eu.verante.trader.indicator.type.IndicatorTypeRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class IndicatorService {

    @Autowired
    private IndicatorRepository indicatorRepository;

    @Autowired
    private IndicatorTypeRegistry indicatorTypeRegistry;

    @Autowired
    private IndicatorMapper indicatorMapper;

    @Autowired
    private ObjectMapper objectMapper;

    public List<Indicator> getAll(Optional<String> type) {
        return indicatorRepository.getAll(type).stream()
                .map(indicatorMapper::toIndicator)
                .collect(Collectors.toList());
    }

    public Indicator getByName(String name) {
        return indicatorMapper.toIndicator(indicatorRepository.getByName(name));
    }

    public void create(CreateIndicatorRequest request) {
        IndicatorItem item = new IndicatorItem();
        item.setName(request.getName());
        item.setDescription(request.getDescription());
        item.setType(request.getIndicatorType());

        Class<IndicatorConfiguration> configurationType = indicatorTypeRegistry.getByName(request.getIndicatorType())
                .getConfigurationType();
        item.setConfigurationType(configurationType);
        item.setConfiguration(convertToConfiguration(request.getConfiguration(), configurationType));

        indicatorRepository.create(item);
    }

    public void update(String name, UpdateIndicatorRequest request) {
        IndicatorItem indicator = indicatorRepository.getByName(name);
        indicator.setDescription(request.getDescription());
        IndicatorConfiguration originalConfig = indicator.getConfiguration();
        Map<String, Object> mergedConfigMap = new HashMap<>(originalConfig.getAsMap());
        mergedConfigMap.putAll(request.getConfiguration());

        indicator.setConfiguration(convertToConfiguration(mergedConfigMap, originalConfig.getClass()));
        indicatorRepository.update(indicator);
    }

    public void delete(String name) {
        indicatorRepository.delete(name);
    }

    public void cleanupIndicators() {
        indicatorRepository.cleanup();
    }

    private IndicatorConfiguration convertToConfiguration(Map<String, Object> configuration,
                                                          Class<? extends IndicatorConfiguration> configurationType) {
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(configuration), configurationType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
