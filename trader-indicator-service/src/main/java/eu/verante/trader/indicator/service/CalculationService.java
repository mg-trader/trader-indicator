package eu.verante.trader.indicator.service;

import eu.verante.trader.indicator.domain.IndicatorValueType;
import eu.verante.trader.indicator.domain.request.CalculationRequest;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.model.item.IndicatorValueItem;
import eu.verante.trader.indicator.model.item.Period;
import eu.verante.trader.indicator.model.repository.IndicatorValueRepository;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcessManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class CalculationService {
    @Autowired
    private AsyncProcessManager processManager;

    @Autowired
    private AsyncProcessHandler<CalculationRequest> calculationHandler;

    @Autowired
    private IndicatorValueRepository indicatorValueRepository;

    @Autowired
    private IndicatorValuesMapper indicatorValuesMapper;

    public AsyncProcess getProcessStatus(UUID processId) {
        return processManager.getById(processId);
    }

    public AsyncProcess calculate(CalculationRequest request) {
        return processManager.runProcess(request, calculationHandler);
    }

    public SortedMap<Long, IndicatorValue> getCalculationResults(CalculationRequest request) {
        List<IndicatorValueItem> entities = indicatorValueRepository.getBySymbolAndPeriodAndIndicator(request.getSymbol(),
                Period.valueOf(request.getPeriod().name()), request.getIndicator(),
                request.getFrom(), request.getTo());

        return new TreeMap<>(entities.stream()
                .filter(entity -> isWithinRange(entity.getTime(), request.getFrom(), request.getTo()))
                .collect(Collectors.toMap(
                        entity -> entity.getTime().toInstant().toEpochMilli(),
                        entity -> indicatorValuesMapper.toValue(entity.getValue(), IndicatorValueType.valueOf(entity.getValueType().name()).getValueClass()))
                ));
    }

    public void cleanup() {
        indicatorValueRepository.cleanup();
    }

    private boolean isWithinRange(ZonedDateTime value, ZonedDateTime from, ZonedDateTime to) {
        if (from != null && !value.isEqual(from) && !value.isAfter(from)) {
            return false;
        }
        if (to!= null && !value.isEqual(to) && !value.isBefore(to)) {
            return false;
        }
        return true;
    }
}
