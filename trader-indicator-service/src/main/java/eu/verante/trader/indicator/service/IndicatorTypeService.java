package eu.verante.trader.indicator.service;

import eu.verante.trader.indicator.domain.IndicatorCategory;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.type.IndicatorCategoryDefinition;
import eu.verante.trader.indicator.type.IndicatorTypeDefinition;
import eu.verante.trader.indicator.type.IndicatorTypeRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class IndicatorTypeService {

    @Autowired
    private IndicatorTypeRegistry registry;

    @Autowired
    private IndicatorMapper mapper;

    public Collection<IndicatorType> getAll(Optional<IndicatorCategory> category) {
        Optional<IndicatorCategoryDefinition> categoryDefinition =
                category.map(c -> IndicatorCategoryDefinition.valueOf(c.name()));
        return registry.getAll().stream()
                .filter(it -> filterByCategory(it, categoryDefinition))
                .map(mapper::toIndicatorType)
                .collect(Collectors.toList());
    }

    public IndicatorType getByName(String type) {
        return mapper.toIndicatorType(registry.getByName(type));
    }

    private boolean filterByCategory(IndicatorTypeDefinition it, Optional<IndicatorCategoryDefinition> category) {
        return category.map(cd -> it.getCategory() == cd).orElse(true);
    }
}
