package eu.verante.trader.indicator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.domain.candle.TradingCandleQuery;
import eu.verante.trader.domain.candle.TradingCandleSummary;
import eu.verante.trader.domain.management.UpdateCandlesRequest;
import eu.verante.trader.domain.management.UpdateStrategy;
import eu.verante.trader.domain.management.UpdateSymbolsRequest;
import eu.verante.trader.indicator.client.TradingCandlesClient;
import eu.verante.trader.indicator.client.TradingManagementClient;
import eu.verante.trader.util.asyncprocess.AsyncProcessAwaitFactory;
import eu.verante.trader.util.asyncprocess.AsyncProcessStatusChecker;
import eu.verante.trader.util.asyncprocess.ProcessStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;

@Component
public class DefaultCandlesRegistryFactory implements CandlesRegistryFactory {

    private final Log logger = LogFactory.getLog(DefaultCandlesRegistryFactory.class);

    @Autowired
    private TradingCandlesClient candlesClient;

    @Autowired
    private TradingManagementClient managementClient;

    @Autowired
    private AsyncProcessAwaitFactory awaitFactory;

    @Override
    @Cacheable("candlesCache")
    public CandlesRegistry prepareRegistry(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        logger.info("Preparing registry for " + symbol);

        ensureCandlesAreLoaded(symbol, period, from, to);

        List<TradingCandle> candles = candlesClient.getCandles(symbol, period, TradingCandleQuery.builder()
                .from(from.toInstant().toEpochMilli())
                .to(to.toInstant().toEpochMilli())
                .build());

        logger.info("Preparation of registry completed for " + symbol);

        return new CandlesRegistry(candles);

    }

    private void ensureCandlesAreLoaded(String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        List<Future<?>> futuresToWait = new ArrayList<>();

        TradingCandleSummary candleSummary = candlesClient.getCandleSummary(symbol, period);
        if (candleSummary.getFromTime() == null || candleSummary.getToTime() == null) {
            logger.info("Candles full update needed for: " + symbol);
            futuresToWait.add(requestSymbolUpdate(symbol));
            futuresToWait.add(requestCandlesUpdate(symbol, period, from, to));
        } else {
            if (candleSummary.getFromTime().isAfter(from)) {
                logger.info("Candles partial update (newest) needed for: " + symbol);
                futuresToWait.add(requestCandlesUpdate(symbol, period, from, candleSummary.getFromTime()));
            }
            if (candleSummary.getToTime().isBefore(to)) {
                logger.info("Candles full update (oldest) needed for: " + symbol);
                futuresToWait.add(requestCandlesUpdate(symbol, period, candleSummary.getToTime(), to));
            }
        }
        futuresToWait.forEach(f -> {
            try {
                f.get();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private Future<ProcessStatus> requestCandlesUpdate(String symbol, Period period, ZonedDateTime from, ZonedDateTime toTime) {
        UUID processId = managementClient.updateCandles(UpdateCandlesRequest.builder()
                .strategy(UpdateStrategy.ADD_NEW)
                .symbols(Collections.singletonList(symbol))
                .periods(Collections.singletonList(period))
                .from(from)
                .to(toTime)
                .build()).getProcessId();
        return awaitFactory.createFor(createStatusChecker(processId));
    }

    private Future<ProcessStatus> requestSymbolUpdate(String symbol) {
        UUID processId = managementClient.updateSymbols(UpdateSymbolsRequest.builder()
                .strategy(UpdateStrategy.ADD_NEW)
                .symbol(symbol)
                .build()).getProcessId();
        return awaitFactory.createFor(createStatusChecker(processId));
    }

    private AsyncProcessStatusChecker createStatusChecker(UUID processId) {
        return () -> managementClient.getProcessStatus(processId);
    }
}
