package eu.verante.trader.indicator;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.indicator.domain.request.CalculationRequest;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.service.CalculationService;
import eu.verante.trader.util.asyncprocess.AsyncProcessAwaitFactory;
import eu.verante.trader.util.asyncprocess.ProcessStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.SortedMap;
import java.util.UUID;
import java.util.concurrent.Future;

@Component
public class DefaultIndicatorValueRegistryFactory implements IndicatorValueRegistryFactory {

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private AsyncProcessAwaitFactory awaitFactory;

    @Override
    public <V extends IndicatorValue> IndicatorValueRegistry<V> prepareRegistry(String indicator, String symbol, Period period, ZonedDateTime from, ZonedDateTime to) {
        CalculationRequest request = CalculationRequest.builder()
                .symbol(symbol)
                .period(period)
                .from(from)
                .to(to)
                .indicator(indicator)
                .build();
        SortedMap<Long, IndicatorValue> results = calculationService.getCalculationResults(request);
        if (results.containsKey(from.toInstant().toEpochMilli())
            && results.containsKey(to.toInstant().toEpochMilli())) {
            return new IndicatorValueRegistry(results);
        }

        awaitIndicatorCalculation(request);
        results = calculationService.getCalculationResults(request);
        return new IndicatorValueRegistry(results);
    }

    private void awaitIndicatorCalculation(CalculationRequest request) {
        UUID processId = calculationService.calculate(request).getProcessId();
        try {
            createStatusChecker(processId).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Future<ProcessStatus> createStatusChecker(UUID processId) {
        return awaitFactory.createFor(() -> calculationService.getProcessStatus(processId));
    }
}
