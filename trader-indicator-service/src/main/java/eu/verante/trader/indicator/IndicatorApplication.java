package eu.verante.trader.indicator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "eu.verante.trader")
@EnableFeignClients
@EnableAsync
@EnableScheduling
@EnableCaching
public class IndicatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndicatorApplication.class);
    }
}
