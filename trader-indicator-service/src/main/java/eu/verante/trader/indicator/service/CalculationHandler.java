package eu.verante.trader.indicator.service;

import eu.verante.trader.indicator.calculator.AbstractIndicatorCalculator;
import eu.verante.trader.indicator.calculator.IndicatorCalculator;
import eu.verante.trader.indicator.domain.request.CalculationRequest;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;
import eu.verante.trader.indicator.model.item.*;
import eu.verante.trader.indicator.model.repository.IndicatorRepository;
import eu.verante.trader.indicator.model.repository.IndicatorValueRepository;
import eu.verante.trader.indicator.type.IndicatorTypeDefinition;
import eu.verante.trader.indicator.type.IndicatorTypeRegistry;
import eu.verante.trader.util.asyncprocess.AbstractAsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CalculationHandler extends AbstractAsyncProcessHandler<CalculationRequest> {

    private final Log logger = LogFactory.getLog(CalculationHandler.class);

    @Autowired
    private IndicatorRepository indicatorRepository;
    @Autowired
    private IndicatorValueRepository indicatorValueRepository;
    @Autowired
    private IndicatorTypeRegistry indicatorTypeRegistry;

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private IndicatorValuesMapper valuesMapper;
    @Override
    protected void doProcess(CalculationRequest request, AsyncProcess process) {
        logger.info("processing calculation for "+ request.getIndicator());
        IndicatorItem indicator = indicatorRepository.getByName(request.getIndicator());
        IndicatorTypeDefinition<IndicatorConfiguration, IndicatorValue, IndicatorCalculator<IndicatorValue>> type =
                indicatorTypeRegistry.getByName(indicator.getType());

        AbstractIndicatorCalculator<IndicatorConfiguration, IndicatorValue> calculator =
                (AbstractIndicatorCalculator<IndicatorConfiguration, IndicatorValue>) beanFactory.getBean(type.getCalculatorType());
        calculator.initialize(indicator.getConfiguration());

        Map<ZonedDateTime, IndicatorValue> values = calculator.calculate(
                request.getSymbol(), request.getPeriod(), request.getFrom(), request.getTo());

        List<IndicatorValueItem> entities = values.entrySet().stream()
                .map(entry -> createEntity(request, entry.getKey(), entry.getValue(), type.getValueType()))
                .collect(Collectors.toList());

        logger.info("calculation completed for: " +request.getIndicator() + "; storing data");
        indicatorValueRepository.create(entities);
        logger.info("calculation completed for: " +request.getIndicator() + "; storing data completed");
    }

    private IndicatorValueItem createEntity(CalculationRequest request, ZonedDateTime key,
                                              IndicatorValue value, IndicatorValueTypeDefinition valueType) {
        IndicatorValueItem entity = new IndicatorValueItem();
        entity.setIndicator(request.getIndicator());
        entity.setPeriod(Period.valueOf(request.getPeriod().name()));
        entity.setSymbol(request.getSymbol());
        entity.setTime(key);
        entity.setValueType(valueType);
        entity.setValue(valuesMapper.toString(value));
        return entity;
    }
}
