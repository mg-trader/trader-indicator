package eu.verante.trader.indicator.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Component
public class IndicatorValuesMapper {

    @Autowired
    private ObjectMapper objectMapper;

    public String toString(IndicatorValue value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public SortedMap<ZonedDateTime, String> toString(Map<ZonedDateTime, IndicatorValue> values) {
        return new TreeMap<>(values.entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> toString(entry.getValue()))));
    }

    public <V extends IndicatorValue> V toValue(String input, Class<V> valueType) {
        try {
            return objectMapper.readValue(input, valueType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
