package eu.verante.trader.indicator.api;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorResource;
import eu.verante.trader.indicator.domain.UpdateIndicatorRequest;
import eu.verante.trader.indicator.domain.request.CreateIndicatorRequest;
import eu.verante.trader.indicator.service.IndicatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/indicator")
public class IndicatorController implements IndicatorResource {

    @Autowired
    private IndicatorService service;

    @Override
    public List<Indicator> getAllIndicators(String type) {
        return service.getAll(Optional.ofNullable(type));
    }


    @Override
    public Indicator getIndicatorByName(String name) {
        return service.getByName(name);
    }

    @Override
    public void createNewIndicator(CreateIndicatorRequest request) {
        service.create(request);
    }

    @Override
    public void updateIndicator(String name, UpdateIndicatorRequest request) {
        service.update(name, request);
    }

    @Override
    public void deleteIndicator(String name) {
        service.delete(name);
    }

    @Override
    public void cleanupIndicators() {
        service.cleanupIndicators();
    }
}
