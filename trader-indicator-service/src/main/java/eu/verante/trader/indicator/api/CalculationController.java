package eu.verante.trader.indicator.api;

import eu.verante.trader.indicator.domain.CalculationResource;
import eu.verante.trader.indicator.domain.request.CalculationRequest;
import eu.verante.trader.indicator.domain.value.IndicatorValue;
import eu.verante.trader.indicator.service.CalculationService;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class CalculationController implements CalculationResource {

    @Autowired
    private CalculationService calculationService;

    @Override
    public AsyncProcess calculate(CalculationRequest request) {
        return calculationService.calculate(request);
    }

    @Override
    public AsyncProcess getProcessStatus(UUID processId) {
        return calculationService.getProcessStatus(processId);
    }

    @Override
    public Map<Long, IndicatorValue> getCalculationResults(CalculationRequest request) {
        return calculationService.getCalculationResults(request);
    }

    @Override
    public void cleanupAllCalculations() {
        calculationService.cleanup();
    }
}
