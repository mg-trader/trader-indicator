package eu.verante.trader.indicator.service;

import eu.verante.trader.indicator.domain.Indicator;
import eu.verante.trader.indicator.domain.IndicatorType;
import eu.verante.trader.indicator.model.item.IndicatorItem;
import eu.verante.trader.indicator.type.IndicatorTypeDefinition;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface IndicatorMapper {

    IndicatorType toIndicatorType(IndicatorTypeDefinition definition);

    @Mapping(target = "configuration",
            expression = "java(item.getConfiguration().getAsMap())")
    Indicator toIndicator(IndicatorItem item);

}
