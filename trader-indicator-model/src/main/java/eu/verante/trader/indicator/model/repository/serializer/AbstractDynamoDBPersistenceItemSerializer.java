package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.item.PersistenceItem;
import software.amazon.awssdk.services.dynamodb.model.AttributeAction;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.AttributeValueUpdate;

import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.collections4.CollectionUtils.intersection;
import static org.apache.commons.collections4.CollectionUtils.subtract;

public abstract class AbstractDynamoDBPersistenceItemSerializer<K, T extends PersistenceItem<K>>
    extends AbstractDynamoDBSerializer<T>
        implements DynamoDBPersistenceItemSerializer<K, T> {

    protected AbstractDynamoDBPersistenceItemSerializer(Class<T> handledType) {
        super(handledType);
    }

    @Override
    public Map<String, AttributeValue> createKey(K key) {
        throw new RuntimeException(getHandledType().getName() + " does not have key");
    }

    @Override
    public Map<String, AttributeValueUpdate> serializeUpdates(T original, T current) {
        Map<String, AttributeValueUpdate> result = new HashMap<>();

        Map<String, AttributeValue> originalSerialized = serialize(original);
        Map<String, AttributeValue> currentSerialized = serialize(current);

        subtract(originalSerialized.keySet(), currentSerialized.keySet())
                .forEach(key -> result.put(key,
                        update(AttributeAction.DELETE, null)));

        subtract(currentSerialized.keySet(), originalSerialized.keySet())
                .forEach(key -> result.put(key,
                        update(AttributeAction.ADD, currentSerialized.get(key))));

        intersection(originalSerialized.keySet(), currentSerialized.keySet()).stream()
                .filter(key -> !currentSerialized.get(key).equals(originalSerialized.get(key)))
                .forEach(key -> result.put(key,
                        update(AttributeAction.PUT, currentSerialized.get(key))));
        return result;
    }

    private AttributeValueUpdate update(AttributeAction action, AttributeValue value) {
        return AttributeValueUpdate.builder()
                .action(action)
                .value(value)
                .build();
    }
}
