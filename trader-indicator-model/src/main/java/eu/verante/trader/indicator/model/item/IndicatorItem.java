package eu.verante.trader.indicator.model.item;

import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;
import lombok.Data;

@Data
public class IndicatorItem
        implements PersistenceItem<String> {

    private String name;

    private String description;
    private String type;

    private Class<? extends IndicatorConfiguration> configurationType;
    private IndicatorConfiguration configuration;

    @Override
    public String getPartitionKey() {
        return type;
    }

}
