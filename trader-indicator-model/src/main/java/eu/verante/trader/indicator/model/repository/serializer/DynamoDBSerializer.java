package eu.verante.trader.indicator.model.repository.serializer;

import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

public interface DynamoDBSerializer<T> {

    Class<T> getHandledType();

    T deserialize(Map<String, AttributeValue> serialized);

    Map<String, AttributeValue> serialize(T item);
}
