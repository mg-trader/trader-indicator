package eu.verante.trader.indicator.model.configuration;

import lombok.Data;

import java.util.Map;

@Data
public class CompareIndicatorConfiguration
        implements IndicatorConfiguration, IndicatorConfigurationKeys {

    private String value;
    private String compareTo;
    @Override
    public Map<String, Object> getAsMap() {
        return Map.of(
                VALUE, value,
                COMPARE_TO, compareTo
        );
    }
}
