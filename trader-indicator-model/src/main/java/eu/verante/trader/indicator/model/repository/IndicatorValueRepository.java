package eu.verante.trader.indicator.model.repository;

import eu.verante.trader.indicator.model.item.IndicatorValueItem;
import eu.verante.trader.indicator.model.item.Period;

import java.time.ZonedDateTime;
import java.util.List;

public interface IndicatorValueRepository extends DynamoDBRepository<String, IndicatorValueItem> {

    List<IndicatorValueItem> getBySymbolAndPeriodAndIndicator(String symbol, Period period, String indicator, ZonedDateTime from, ZonedDateTime to);

    void cleanup();
}
