package eu.verante.trader.indicator.model.configuration;

import lombok.Data;

import java.util.Collections;
import java.util.Map;

@Data
public class SimpleIndicatorConfiguration
        implements IndicatorConfiguration, IndicatorConfigurationKeys {

    private SimpleValueTypeDefinition value;

    @Override
    public Map<String, Object> getAsMap() {
        return Collections.singletonMap(VALUE, value);
    }
}
