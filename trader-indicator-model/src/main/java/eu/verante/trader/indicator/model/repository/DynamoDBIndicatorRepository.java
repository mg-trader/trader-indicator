package eu.verante.trader.indicator.model.repository;

import eu.verante.trader.indicator.model.item.IndicatorItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class DynamoDBIndicatorRepository
        extends AbstractDynamoDBRepository<String, IndicatorItem>
        implements IndicatorRepository {

    private final Log logger = LogFactory.getLog(DynamoDBIndicatorRepository.class);
    private static final String INDICATOR_TABLE = "Indicator";

    protected DynamoDBIndicatorRepository() {
        super(INDICATOR_TABLE);
    }

    @Override
    public IndicatorItem getByName(String name) {
        List<IndicatorItem> items = queryByKey("name", name, "name_index");
        if (items.size() == 1) {
            return items.get(0);
        }
        throw new RuntimeException("Searching indicator by name: " + name + " resulted with invalud number of results: " + items.size());
    }

    @Override
    public List<IndicatorItem> getAll(Optional<String> type) {
        if (type.isPresent()) {
            return queryByKey("type", type.get());
        }

        ScanRequest.Builder builder = ScanRequest.builder()
                .tableName(getTableName());
        List<Map<String, AttributeValue>> items = getDbClient().scan(builder
                .build()).items();

        return deserialize(items);
    }

    @Override
    public void cleanup() {
        try {
            getDbClient().deleteTable(DeleteTableRequest.builder()
                    .tableName(getTableName())
                    .build());
        } catch (ResourceNotFoundException e) {
            logger.warn("Deleting not existent table: " + getTableName());
        }

        createTable();
    }

    private void applyTypeFilter(ScanRequest.Builder builder, String t) {
        builder.filterExpression("#type = :type")
                .expressionAttributeNames(Map.of("#type", "type"))
                .expressionAttributeValues(Map.of(":type", AttributeValue.fromS(t)));
    }

    private void createTable() {
        List<AttributeDefinition> attributeDefinitions= new ArrayList<AttributeDefinition>();
        attributeDefinitions.add(AttributeDefinition.builder()
                .attributeName("type")
                .attributeType(ScalarAttributeType.S)
                .build());
        attributeDefinitions.add(AttributeDefinition.builder()
                .attributeName("name")
                .attributeType(ScalarAttributeType.S)
                .build());

        List<KeySchemaElement> keySchema = new ArrayList<>();
        keySchema.add(KeySchemaElement.builder()
                .attributeName("type")
                .keyType(KeyType.HASH)
                .build());
        keySchema.add(KeySchemaElement.builder()
                .attributeName("name")
                .keyType(KeyType.RANGE)
                .build());

        GlobalSecondaryIndex index = GlobalSecondaryIndex.builder()
                .indexName("name_index")
                .keySchema(List.of(
                        KeySchemaElement.builder()
                                .attributeName("name")
                                .keyType(KeyType.HASH)
                                .build()
                ))
                .projection(Projection.builder()
                        .projectionType(ProjectionType.ALL)
                        .build())
                .build();

        CreateTableRequest request = CreateTableRequest.builder()
                .tableName(getTableName())
                .keySchema(keySchema)
                .globalSecondaryIndexes(index)
                .attributeDefinitions(attributeDefinitions)
                .billingMode(BillingMode.PAY_PER_REQUEST)
                .build();
        getDbClient().createTable(request);
    }
}
