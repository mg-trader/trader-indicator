package eu.verante.trader.indicator.model.configuration;

public interface IndicatorConfigurationKeys {

    String PERIODS = "periods";
    String SHIFT = "shift";
    String BASE = "base";
    String VALUE = "value";
    String INDICATOR = "indicator";
    String THRESHOLD = "threshold";
    String COMPARE_TO = "compareTo";
}
