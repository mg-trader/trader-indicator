package eu.verante.trader.indicator.model.configuration;

import java.util.Map;

public interface IndicatorConfiguration {

    Map<String, Object> getAsMap();

}
