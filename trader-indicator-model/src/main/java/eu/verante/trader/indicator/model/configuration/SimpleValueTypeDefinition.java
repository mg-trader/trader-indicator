package eu.verante.trader.indicator.model.configuration;

public enum SimpleValueTypeDefinition {

    OPEN,
    CLOSE,
    HIGH,
    LOW,
    TYPICAL,
    VOLUME
}
