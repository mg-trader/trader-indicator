package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.configuration.IndicatorConfiguration;
import eu.verante.trader.indicator.model.item.IndicatorItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class IndicatorItemSerializer
        extends AbstractDynamoDBPersistenceItemSerializer<String, IndicatorItem> {

    private Map<Class<?>, DynamoDBSerializer<?>> configurationSerializers;

    protected IndicatorItemSerializer() {
        super(IndicatorItem.class);
    }

    @Autowired
    public void initializeConfigSerializers(List<DynamoDBSerializer> allSerializers) {
        configurationSerializers = allSerializers.stream()
                .filter(ser -> IndicatorConfiguration.class.isAssignableFrom(ser.getHandledType()))
                .collect(Collectors.toMap(
                        DynamoDBSerializer::getHandledType,
                        ser -> ser)
                );
    }

    @Override
    public IndicatorItem deserialize(Map<String, AttributeValue> serialized) {
        IndicatorItem item = new IndicatorItem();
        item.setName(serialized.get("name").s());
        item.setDescription(serialized.get("description").s());
        item.setType(serialized.get("type").s());
        item.setConfigurationType(getConfigurationType(serialized));

        item.setConfiguration(deserializeConfiguration(serialized, item.getConfigurationType()));
        return item;
    }

    private static Class<? extends IndicatorConfiguration> getConfigurationType(Map<String, AttributeValue> serialized) {
        try {
            return (Class<? extends IndicatorConfiguration>) Class.forName(serialized.get("configurationType").s());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, AttributeValue> serialize(IndicatorItem item) {
        Map<String, AttributeValue> serialized = new HashMap<>();
        serialized.put("name", string(item.getName()));
        serialized.put("description", string(item.getDescription()));
        serialized.put("type", string(item.getType()));
        serialized.put("configurationType", string(item.getConfigurationType().getName()));

        serialized.put("config", nested(serializeConfiguration(item.getConfiguration())));
        return serialized;
    }

    @Override
    public Map<String, AttributeValue> createKey(String key) {
        return Map.of("name", string(key));
    }

    private Map<String, AttributeValue> serializeConfiguration(IndicatorConfiguration configuration) {
        DynamoDBSerializer<IndicatorConfiguration> configSerializer =
                (DynamoDBSerializer<IndicatorConfiguration>) configurationSerializers.get(configuration.getClass());
        return configSerializer.serialize(configuration);
    }

    private IndicatorConfiguration deserializeConfiguration(Map<String, AttributeValue> serialized,
                                                            Class<? extends IndicatorConfiguration> configurationType) {
        Map<String, AttributeValue> configuration = serialized.get("config").m();

        DynamoDBSerializer<?> configSerializer = configurationSerializers.get(configurationType);
        return (IndicatorConfiguration) configSerializer.deserialize(configuration);
    }
}
