package eu.verante.trader.indicator.model.repository.serializer;

import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

public abstract class AbstractDynamoDBSerializer<T> implements DynamoDBSerializer<T> {

    private final Class<T> handledType;

    protected AbstractDynamoDBSerializer(Class<T> handledType) {
        this.handledType = handledType;
    }

    @Override
    public Class<T> getHandledType() {
        return handledType;
    }

    protected AttributeValue string(String value) {
        return AttributeValue.builder()
                .s(value)
                .build();
    }

    protected AttributeValue nested(Map<String, AttributeValue> value) {
        return AttributeValue.builder()
                .m(value)
                .build();
    }

    protected AttributeValue number(Number value) {
        return AttributeValue.builder()
                .n("" + value)
                .build();
    }
}
