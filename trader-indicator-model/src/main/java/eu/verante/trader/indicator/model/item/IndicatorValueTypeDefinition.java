package eu.verante.trader.indicator.model.item;

public enum IndicatorValueTypeDefinition {
    SIMPLE,
    TREND,
    COMPARE
}
