package eu.verante.trader.indicator.model.repository;

import eu.verante.trader.indicator.model.item.IndicatorItem;

import java.util.List;
import java.util.Optional;

public interface IndicatorRepository extends DynamoDBRepository<String, IndicatorItem> {
    IndicatorItem getByName(String name);

    List<IndicatorItem> getAll(Optional<String> type);

    void cleanup();
}
