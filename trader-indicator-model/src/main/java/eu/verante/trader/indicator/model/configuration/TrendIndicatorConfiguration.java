package eu.verante.trader.indicator.model.configuration;

import lombok.Data;

import java.util.Map;

@Data
public class TrendIndicatorConfiguration
        implements IndicatorConfiguration, IndicatorConfigurationKeys {

    private String indicator;
    private Double threshold;

    @Override
    public Map<String, Object> getAsMap() {
        return Map.of(
                INDICATOR, indicator,
                THRESHOLD, threshold
        );
    }
}
