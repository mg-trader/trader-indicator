package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.item.IndicatorItem;
import eu.verante.trader.indicator.model.item.IndicatorValueItem;
import eu.verante.trader.indicator.model.item.IndicatorValueTypeDefinition;
import eu.verante.trader.indicator.model.item.Period;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

@Component
public class IndicatorValueItemSerializer extends AbstractDynamoDBPersistenceItemSerializer<String, IndicatorValueItem> {
    protected IndicatorValueItemSerializer() {
        super(IndicatorValueItem.class);
    }

    @Override
    public IndicatorValueItem deserialize(Map<String, AttributeValue> serialized) {
        IndicatorValueItem item = new IndicatorValueItem();
        item.setIndicator(serialized.get("indicator").s());
        item.setSymbol(serialized.get("symbol").s());
        item.setPeriod(Period.valueOf(serialized.get("period").s()));
        item.setTime(Instant.ofEpochMilli(Long.valueOf(serialized.get("time").n())).atZone(ZoneId.systemDefault()));
        item.setValueType(IndicatorValueTypeDefinition.valueOf(serialized.get("valueType").s()));
        item.setValue(serialized.get("value").s());
        return item;
    }

    @Override
    public Map<String, AttributeValue> serialize(IndicatorValueItem item) {
        Map<String, AttributeValue> serialized = new HashMap<>();
        serialized.put("id", string(item.getPartitionKey()));
        serialized.put("indicator", string(item.getIndicator()));
        serialized.put("symbol", string(item.getSymbol()));
        serialized.put("period", string(item.getPeriod().name()));
        serialized.put("time", number(item.getTime().toInstant().toEpochMilli()));
        serialized.put("valueType", string(item.getValueType().name()));
        serialized.put("value", string(item.getValue()));
        return serialized;
    }
}
