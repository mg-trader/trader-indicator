package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.configuration.IndicatorConfigurationKeys;
import eu.verante.trader.indicator.model.configuration.MAIndicatorConfiguration;
import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

@Component
public class SMAIndicatorConfigurationSerializer extends AbstractDynamoDBSerializer<MAIndicatorConfiguration>
        implements IndicatorConfigurationKeys {
    protected SMAIndicatorConfigurationSerializer() {
        super(MAIndicatorConfiguration.class);
    }

    @Override
    public MAIndicatorConfiguration deserialize(Map<String, AttributeValue> serialized) {
        MAIndicatorConfiguration result = new MAIndicatorConfiguration();
        result.setPeriods(Integer.parseInt(serialized.get(PERIODS).n()));
        result.setShift(Integer.parseInt(serialized.get(SHIFT).n()));
        result.setBase(SimpleValueTypeDefinition.valueOf(serialized.get(BASE).s()));
        return result;
    }

    @Override
    public Map<String, AttributeValue> serialize(MAIndicatorConfiguration item) {
        return Map.of(
                PERIODS, number(item.getPeriods()),
                SHIFT, number(item.getShift()),
                BASE, string(item.getBase().name())
        );
    }
}
