package eu.verante.trader.indicator.model.repository;

import eu.verante.trader.indicator.model.item.IndicatorValueItem;
import eu.verante.trader.indicator.model.item.Period;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class DynamoDBIndicatorValueRepository extends AbstractDynamoDBRepository<String, IndicatorValueItem>
        implements IndicatorValueRepository {
    private final Log logger = LogFactory.getLog(DynamoDBIndicatorValueRepository.class);

    protected DynamoDBIndicatorValueRepository() {
        super("IndicatorValue");
    }

    @Override
    public List<IndicatorValueItem> getBySymbolAndPeriodAndIndicator(String symbol, Period period, String indicator,
                                                                     ZonedDateTime from, ZonedDateTime to) {
        Long fromValue = from != null ? from.toInstant().toEpochMilli() : 0L;
        Long toValue = from != null ? to.toInstant().toEpochMilli() : Long.MAX_VALUE;
        Map<String, Condition> additionalConditions = Map.of("time", Condition.builder()
                .comparisonOperator(ComparisonOperator.BETWEEN)
                .attributeValueList(
                        AttributeValue.builder().n("" + fromValue).build(),
                        AttributeValue.builder().n("" + toValue).build())
                .build());
        return queryByKey("id", symbol + "|" + period + "|" + indicator, Optional.empty(), additionalConditions);
    }

    @Override
    public void cleanup() {
        try {
            getDbClient().deleteTable(DeleteTableRequest.builder()
                    .tableName(getTableName())
                    .build());
        } catch (ResourceNotFoundException e) {
            logger.warn("Deleting not existent table: " + getTableName());
        }

        createTable();
    }

    private void createTable() {
        List<AttributeDefinition> attributeDefinitions= new ArrayList<AttributeDefinition>();
        attributeDefinitions.add(AttributeDefinition.builder()
                .attributeName("id")
                .attributeType(ScalarAttributeType.S)
                .build());
        attributeDefinitions.add(AttributeDefinition.builder()
                .attributeName("time")
                .attributeType(ScalarAttributeType.N)
                .build());

        List<KeySchemaElement> keySchema = new ArrayList<>();
        keySchema.add(KeySchemaElement.builder()
                .attributeName("id")
                .keyType(KeyType.HASH)
                .build());
        keySchema.add(KeySchemaElement.builder()
                .attributeName("time")
                .keyType(KeyType.RANGE)
                .build());

        CreateTableRequest request = CreateTableRequest.builder()
                .tableName(getTableName())
                .keySchema(keySchema)
                .attributeDefinitions(attributeDefinitions)
                .billingMode(BillingMode.PAY_PER_REQUEST)
                .build();
        getDbClient().createTable(request);
    }
}
