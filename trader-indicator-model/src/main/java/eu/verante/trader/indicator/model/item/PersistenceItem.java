package eu.verante.trader.indicator.model.item;

import java.util.Optional;

public interface PersistenceItem<K> {

    K getPartitionKey();
}
