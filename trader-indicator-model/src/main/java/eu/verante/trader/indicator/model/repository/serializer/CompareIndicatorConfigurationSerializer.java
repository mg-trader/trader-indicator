package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.configuration.CompareIndicatorConfiguration;
import eu.verante.trader.indicator.model.configuration.IndicatorConfigurationKeys;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

@Component
public class CompareIndicatorConfigurationSerializer  extends AbstractDynamoDBSerializer<CompareIndicatorConfiguration>
        implements IndicatorConfigurationKeys {

    protected CompareIndicatorConfigurationSerializer() {
        super(CompareIndicatorConfiguration.class);
    }

    @Override
    public CompareIndicatorConfiguration deserialize(Map<String, AttributeValue> serialized) {
        CompareIndicatorConfiguration configuration = new CompareIndicatorConfiguration();
        configuration.setValue(serialized.get(VALUE).s());
        configuration.setCompareTo(serialized.get(COMPARE_TO).s());
        return configuration;
    }

    @Override
    public Map<String, AttributeValue> serialize(CompareIndicatorConfiguration item) {
        return Map.of(
                VALUE, string(item.getValue()),
                COMPARE_TO, string(item.getCompareTo())
        );
    }
}
