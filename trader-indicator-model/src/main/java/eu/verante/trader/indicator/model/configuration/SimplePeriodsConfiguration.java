package eu.verante.trader.indicator.model.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimplePeriodsConfiguration implements IndicatorConfiguration, IndicatorConfigurationKeys {

    private int periods;

    @Override
    public Map<String, Object> getAsMap() {
        return Map.of(PERIODS, periods);
    }
}
