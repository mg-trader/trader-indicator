package eu.verante.trader.indicator.model.repository;

import eu.verante.trader.indicator.model.item.PersistenceItem;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DynamoDBRepository<K, T extends PersistenceItem<K>> {

    String getTableName();

    void create(T element);

    void create(Collection<T> elements);

    List<T> getAll();

    Optional<T> getByKey(K key);
    void update(T element);

    void delete(K key);

}
