package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.configuration.IndicatorConfigurationKeys;
import eu.verante.trader.indicator.model.configuration.TrendIndicatorConfiguration;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

@Component
public class TrendIndicatorConfigurationSerializer extends AbstractDynamoDBSerializer<TrendIndicatorConfiguration>
        implements IndicatorConfigurationKeys {

    protected TrendIndicatorConfigurationSerializer() {
        super(TrendIndicatorConfiguration.class);
    }

    @Override
    public TrendIndicatorConfiguration deserialize(Map<String, AttributeValue> serialized) {
        TrendIndicatorConfiguration configuration = new TrendIndicatorConfiguration();
        configuration.setIndicator(serialized.get(INDICATOR).s());
        configuration.setThreshold(Double.parseDouble(serialized.get(THRESHOLD).n()));
        return configuration;
    }

    @Override
    public Map<String, AttributeValue> serialize(TrendIndicatorConfiguration item) {
        return Map.of(
                INDICATOR, string(item.getIndicator()),
                THRESHOLD, number(item.getThreshold())
        );
    }
}
