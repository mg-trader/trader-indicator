package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.configuration.SimpleIndicatorConfiguration;
import eu.verante.trader.indicator.model.configuration.SimplePeriodsConfiguration;
import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

import static eu.verante.trader.indicator.model.configuration.IndicatorConfigurationKeys.PERIODS;

@Component
public class SimplePeriodsConfigurationSerializer extends AbstractDynamoDBSerializer<SimplePeriodsConfiguration> {
    protected SimplePeriodsConfigurationSerializer() {
        super(SimplePeriodsConfiguration.class);
    }

    @Override
    public SimplePeriodsConfiguration deserialize(Map<String, AttributeValue> serialized) {
        SimplePeriodsConfiguration configuration = new SimplePeriodsConfiguration();
        configuration.setPeriods(Integer.valueOf(serialized.get(PERIODS).n()));
        return configuration;
    }

    @Override
    public Map<String, AttributeValue> serialize(SimplePeriodsConfiguration item) {
        return Map.of(
                PERIODS, number(item.getPeriods())
        );
    }
}
