package eu.verante.trader.indicator.model.repository;

import eu.verante.trader.indicator.model.item.PersistenceItem;
import eu.verante.trader.indicator.model.repository.serializer.DynamoDBPersistenceItemSerializer;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractDynamoDBRepository<K, T extends PersistenceItem<K>>
        implements DynamoDBRepository<K, T> {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private DynamoDbClient dbClient;

    @Autowired
    private DynamoDBPersistenceItemSerializer<K, T> serializer;

    private final String tableName;

    protected AbstractDynamoDBRepository(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public void create(T element) {
        dbClient.putItem(generatePutRequest(element));
    }

    @Override
    public void create(Collection<T> elements) {
        List<WriteRequest> requests = generatePutRequests(elements);
        List<List<WriteRequest>> partitions = ListUtils.partition(requests, 25);
        partitions.parallelStream().forEach(partition -> {
            BatchWriteItemRequest request = BatchWriteItemRequest.builder()
                    .requestItems(Map.of(getTableName(), partition))
                    .build();
            dbClient.batchWriteItem(request);
        });

    }

    private List<WriteRequest> generatePutRequests(Collection<T> elements) {
        return elements.stream()
                .map(serializer::serialize)
                .map(ser -> PutRequest.builder()
                        .item(ser)
                        .build())
                .map(put -> WriteRequest.builder()
                        .putRequest(put)
                        .build())
                .collect(Collectors.toList());
    }

    private PutItemRequest generatePutRequest(T element) {
        Map<String, AttributeValue> serialized = serializer.serialize(element);
        return PutItemRequest.builder()
                .tableName(getTableName())
                .item(serialized)
                .build();
    }

    @Override
    public List<T> getAll() {
        List<Map<String, AttributeValue>> items = dbClient.scan(ScanRequest.builder()
                .tableName(getTableName())
                .build()).items();

        return deserialize(items);
    }

    @Override
    public Optional<T> getByKey(K key) {
        GetItemResponse response = dbClient.getItem(GetItemRequest.builder()
                .tableName(getTableName())
                .key(serializer.createKey(key))
                .build());
        return response.hasItem() ?
                Optional.of(serializer.deserialize(response.item())) :
                Optional.empty();
    }

    @Override
    public void update(T item) {
        T original = getByKey(item.getPartitionKey()).orElseThrow();
        dbClient.updateItem(UpdateItemRequest.builder()
                .tableName(getTableName())
                .key(serializer.createKey(item.getPartitionKey()))
                .attributeUpdates(serializer.serializeUpdates(original, item))
                .build());
    }

    @Override
    public void delete(K key) {
        dbClient.deleteItem(DeleteItemRequest.builder()
                .tableName(getTableName())
                .key(serializer.createKey(key))
                .build());
    }

    protected DynamoDbClient getDbClient() {
        return dbClient;
    }

    protected List<T> deserialize(List<Map<String, AttributeValue>> items) {
        if (items == null) {
            return List.of();
        }
        return items.stream()
                .map(serializer::deserialize)
                .collect(Collectors.toList());
    }

    protected List<T> queryByKey(String keyName, String value) {
        return queryByKey(keyName, value, Optional.empty(), Map.of());
    }

    protected List<T> queryByKey(String keyName, String value, String indexToUse) {
        return queryByKey(keyName, value, Optional.of(indexToUse), Map.of());
    }

    protected List<T> queryByKey(String keyName, String value, Optional<String> indexToUse, Map<String, Condition> additionalConditions) {
        Map<String, Condition> finalConditions = new HashMap<>();
        finalConditions.put(keyName, Condition.builder()
                .comparisonOperator(ComparisonOperator.EQ)
                .attributeValueList(AttributeValue.builder()
                        .s(value)
                        .build())
                .build());
        finalConditions.putAll(additionalConditions);

        QueryRequest.Builder request = QueryRequest.builder()
                .tableName(getTableName())
                .keyConditions(finalConditions);
        indexToUse.ifPresent( i -> request.indexName(i));
        List<Map<String, AttributeValue>> items = new ArrayList();
        boolean proceed = true;
        while (proceed) {
            QueryResponse response = getDbClient().query(request.build());
            items.addAll(response.items());
            if (response.hasLastEvaluatedKey()) {
                request.exclusiveStartKey(response.lastEvaluatedKey());
            } else {
                proceed = false;
            }
        }
        return deserialize(items);
    }
}


