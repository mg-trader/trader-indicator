package eu.verante.trader.indicator.model.item;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class IndicatorValueItem implements PersistenceItem<String>{

    private String indicator;
    private String symbol;
    private Period period;

    private ZonedDateTime time;

    private IndicatorValueTypeDefinition valueType;

    private String value;

    @Override
    public String getPartitionKey() {
        return symbol+"|"+period+"|"+indicator;
    }
}
