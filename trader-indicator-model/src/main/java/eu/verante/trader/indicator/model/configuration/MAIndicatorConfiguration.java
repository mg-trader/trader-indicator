package eu.verante.trader.indicator.model.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MAIndicatorConfiguration
        implements IndicatorConfiguration, IndicatorConfigurationKeys {
    private int periods;

    private int shift=0;

    private SimpleValueTypeDefinition base = SimpleValueTypeDefinition.CLOSE;

    @Override
    public Map<String, Object> getAsMap() {
        return Map.of(PERIODS, periods,
                SHIFT, shift,
                BASE, base);
    }
}
