package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.configuration.IndicatorConfigurationKeys;
import eu.verante.trader.indicator.model.configuration.SimpleIndicatorConfiguration;
import eu.verante.trader.indicator.model.configuration.SimpleValueTypeDefinition;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

@Component
public class SimpleIndicatorConfigurationSerializer extends AbstractDynamoDBSerializer<SimpleIndicatorConfiguration>
        implements IndicatorConfigurationKeys {
    protected SimpleIndicatorConfigurationSerializer() {
        super(SimpleIndicatorConfiguration.class);
    }

    @Override
    public SimpleIndicatorConfiguration deserialize(Map<String, AttributeValue> serialized) {
        SimpleIndicatorConfiguration configuration = new SimpleIndicatorConfiguration();
        configuration.setValue(SimpleValueTypeDefinition.valueOf(serialized.get(VALUE).s()));
        return configuration;
    }

    @Override
    public Map<String, AttributeValue> serialize(SimpleIndicatorConfiguration item) {
        return Map.of(
                VALUE, string(item.getValue().name())
        );
    }
}
