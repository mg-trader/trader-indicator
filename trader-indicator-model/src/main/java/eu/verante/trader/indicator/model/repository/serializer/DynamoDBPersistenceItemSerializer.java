package eu.verante.trader.indicator.model.repository.serializer;

import eu.verante.trader.indicator.model.item.PersistenceItem;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.AttributeValueUpdate;

import java.util.Map;

public interface DynamoDBPersistenceItemSerializer<K, T extends PersistenceItem<K>>
        extends DynamoDBSerializer<T>{

    Map<String, AttributeValue> createKey(K key);

    Map<String, AttributeValueUpdate> serializeUpdates(T original, T current);


}
